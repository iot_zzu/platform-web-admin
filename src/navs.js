// <!-- auto generated navs start -->
const autoGenHeaderNavs = [];
const autoGenAsideNavs = [];

// <!-- auto generated navs end -->

const customHeaderNavs = [
  {
    text: '首页',
    to: '/',
    icon: 'home',
  },
  {
    text: '反馈',
    to: 'https://github.com/alibaba/ice',
    external: true,
    newWindow: true,
    icon: 'message',
  },
  {
    text: '帮助',
    to: 'https://alibaba.github.io/ice',
    external: true,
    newWindow: true,
    icon: 'bangzhu',
  },
];

const customAsideNavs = [
  {
    text: '首页',
    to: '/',
    icon: 'home',
  },
  {
    text: '规格管理',
    to: '/spec',
    icon: 'copy',
    children: [
      { text: '字段管理', to: '/spec/field' },
      { text: '规格列表', to: '/spec/list' },
      { text: '添加规格', to: '/spec/add', icon: 'nav-list' },
    
    ],
  },
  {
    text: '设备管理',
    to: '/device',
    icon: 'cascades',
    children: [
      { text: '设备列表', to: '/device/list' },
    ],
  },
  {
    text: '用户管理',
    to: '/user',
    icon: 'pin',
    children: [
      { text: '用户列表', to: '/user/list' },
    ],
  },
  {
    text: '版本管理',
    to: '/version',
    icon: 'yonghu',
    children: [
      { text: '版本列表', to: '/version/list' },
    ],
  },
];

function transform(navs) {
  // custom logical
  return [...navs];
}

export const headerNavs = transform([
  ...autoGenHeaderNavs,
  ...customHeaderNavs,
]);

export const asideNavs = transform([...autoGenAsideNavs, ...customAsideNavs]);

import React, { Component } from 'react';
import { Card } from '@icedesign/base';
export default class MainData extends Component {
  static displayName = 'MainData';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Card style={{width:'100%',borderRadius:'6px'}} bodyHeight="auto">
        <div style={styles.wrapper}>
          <div style={styles.content}>
            <div style={styles.contentItem}>
              <div style={styles.contentNum}>
                <span style={styles.bigNum}>设备总量</span>
                <span style={styles.symbol}></span>
              </div>
              <div style={styles.contentDesc}>20个设备</div>
            </div>
            <div style={styles.contentItem}>
              <div style={styles.contentNum}>
                <span style={styles.bigNum}>异常设备</span>
                <span style={styles.symbol}></span>
              </div>
              <div style={styles.contentDesc}>0次</div>
            </div>
            <div style={styles.contentItem}>
              <div style={styles.contentNum}>
                <span style={styles.bigNum}>运行设备</span>
                <span style={styles.symbol}></span>
              </div>
              <div style={styles.contentDesc}>10个设备</div>
            </div>
            <div style={styles.contentItem}>
              <div style={styles.contentNum}>
                <span style={styles.bigNum}>设备分布</span>
                <span style={styles.symbol}></span>
              </div>
              <div style={styles.contentDesc}>10个地区</div>
            </div>
          </div>
        </div>
      </Card>
    );
  }
}

const styles = {
  wrapper: {
    background: '#fff',
    borderRadius: '6px'
  },
  content: {
    width: '100%',
    height: 150,
    maxWidth: 1024,
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    margin: '0 auto',
  },
  contentItem: {},
  contentNum: {
    display: 'flex',
    alignItems: 'center',
  },
  bigNum: {
    color: 'rgb(141,141,141)',
    fontSize: 13,
  },
  symbol: {
    color: '#333',
    fontSize: 30,
    marginLeft: 10,
  },
  contentDesc: {
    color: '#666666',
    fontSize: 20,
    textAlign: 'center',
    marginTop: 13,
  },
};

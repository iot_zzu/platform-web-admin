import React, { Component } from 'react';
import IceContainer from '@icedesign/container';
import {
  Grid,
  Card
} from '@icedesign/base'
const { Row, Col } = Grid;

const dataSource3 = [];
const dataSource = [];
const dataSource2 = [];
for (let i = 0; i < 5; i++) {
  dataSource3.push({
    message:
      <Row>
        <Col l={6} xxs={24} s={24}><b>硬件损坏</b>无法检测到温度传感器，错误码：012013</Col>
      </Row>,
  })

  dataSource.push({
    message:
      <Row>
        <Col l={6} xxs={24} s={24}>2018-05-06</Col>
        <Col l={6} xxs={24} s={24}>192.168.128.56</Col>
        <Col l={6} xxs={24} s={24}>河南省/郑州市/金水区/郑州大学北校区</Col>
      </Row>,
  })
  dataSource2.push({
    message:
      <Row>
        <Col l={6} xxs={24} s={24}>2018-05-06</Col>
        <Col l={6} xxs={24} s={24}>1201212031234</Col>
        <Col l={6} xxs={24} s={24}>CBH</Col>
        <Col l={6} xxs={24} s={24}>15617817579</Col>
        <Col l={6} xxs={24} s={24}>15617817579@163.com</Col>
        <Col l={6} xxs={24} s={24}>河南省/郑州市/金水区</Col>
      </Row>,
  })
}

export default class FeedList extends Component {
  static displayName = 'FeedList';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  // ICE: React Component 的生命周期

  componentWillMount() { }

  componentDidMount() { }

  componentWillUnmount() { }
  renderItem = (item, idx) => {
    return (
      <div style={styles.item} key={idx}>
        <a href="##" style={styles.message}>
          {item.message}
        </a>
      </div>
    );
  };

  render() {
    return (
      <div className="feed-list">
        <IceContainer>
          <Card style={{ width: '100%', borderRadius:'6px', marginTop: '20px' }} bodyHeight="auto">
            <div style={styles.titleRow}>
              <h2 style={styles.cardTitle}>异常信息</h2>
              <span style={styles.status}>共10条记录</span>
            </div>
            {dataSource3.map(this.renderItem)}
            <div style={styles.allMessage}>
              <a href="##">查看全部记录</a>
            </div>
          </Card>
          <Card style={{ width: '100%', borderRadius:'6px', marginTop: '20px' }} bodyHeight="auto">
            <div style={styles.titleRow}>
              <h2 style={styles.cardTitle}>登陆信息</h2>
              <span style={styles.status}>共10条记录</span>
            </div>
            {dataSource.map(this.renderItem)}
            <div style={styles.allMessage}>
              <a href="##">查看全部记录</a>
            </div>
          </Card>
          <Card style={{ width: '100%', borderRadius:'6px', marginTop: '20px' }} bodyHeight="auto">
            <div style={styles.titleRow}>
              <h2 style={styles.cardTitle}>绑定信息</h2>
              <span style={styles.status}>共10条记录</span>
            </div>
            {dataSource2.map(this.renderItem)}
            <div style={styles.allMessage}>
              <a href="##">查看全部记录</a>
            </div>
          </Card>
        </IceContainer>
      </div>
    );
  }
}

const styles = {
  titleRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: '15px',
  },
  cardTitle: {
    margin: 0,
    fontSize: '18px',
    display: 'inline-flex',
  },
  title: {
    fontSize: '14px',
    display: 'inline-flex',
    lineHeight: '22px',
  },
  status: {
    display: 'flex',
    alignItems: 'center',
    color: '#999',
    fontSize: '12px',
  },
  itemRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  avatar: {
    width: '24px',
    height: '24px',
    borderRadius: '50%',
    marginRight: '10px',
  },
  item: {
    display: 'flex',
    flexDirection: 'column',
    paddingTop: '15px',
    borderBottom: '1px solid #fafafa',
  },
  message: {
    color: '#999',
    fontSize: '12px',
    paddingLeft: '34px',
    marginBottom: '15px',
    lineHeight: '22px',
  },
  allMessage: {
    textAlign: 'center',
    height: '50px',
    lineHeight: '50px',
  },
};

import React, { Component } from 'react';
import UserStatChart from './components/UserStatChart';
import ChartTypeLine from './components/ChartTypeLine';
import DataDisplay from './components/DataDisplay';
import FeedList from './components/FeedList';
import { Link } from 'react-router';
import { Grid, Button, Notice, Icon, Card, Tab, Tag, Table, Switch } from '@icedesign/base';
import MainData from './components/MainData/index';
const TabPane = Tab.TabPane;
const { Row, Col } = Grid;

const data = {
  name: 'sun',
  uid: '572220216',
  phone: '18638729321',
  email: '572220216@qq.com',
  address: '河南省郑州市金水区',
  sid: 'spec-spec-spec-spec-spec',
  time: '2018-05-06',
  token: 'boxa-boxa-boxa-01-0000001',
  dstate: '已绑定',
  type: '生活/居家/雾霾',
  tag: <Link size="small">标签</Link>,
  runstate: '运行中',
  note: '雾霾盒子是真的好',
};

export default class DevicesDetail extends Component {
  static displayName = 'DevicesDetail';

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const styles = {
      basicDetailTitle: {
        margin: '10px 0',
        fontSize: '16px',
      },
      infoColumn: {
        marginLeft: '16px',
      },
      infoColumnTitle: {
        margin: '20px 0',
        paddingLeft: '10px',
        borderLeft: '3px solid #3080fe',
      },
      infoItems: {
        padding: 0,
        marginLeft: '25px',
      },
      infoItem: {
        marginBottom: '18px',
        listStyle: 'none',
        fontSize: '14px',
      },
      infoItemLabel: {
        minWidth: '70px',
        color: '#999',
      },
      infoItemValue: {
        color: '#333',
      },
      attachLabel: {
        minWidth: '70px',
        color: '#999',
        float: 'left',
      },
      attachPics: {
        width: '80px',
        height: '80px',
        border: '1px solid #eee',
        marginRight: '10px',
      },
      bigNum: {
        color: 'rgb(141,141,141)',
        fontSize: 13,
      },
      symbol: {
        color: '#333',
        fontSize: 30,
        marginLeft: 10,
      },

    };
    const tabs = [
      {
        tab: "总览", key: "summary", content: <div>
          <Row>
            <Col span={24}>
              <Notice style={{ marginBottom: '10px' }} title="设备异常" type="warning" closable>
                <ul style={{ fontSize: "16px" }}>
                  <li>异常事件: 2018-4-30</li>
                  <li>异常信息:xxxxxxxxxxxxxx</li>
                </ul>
              </Notice>
              <Card bodyHeight="auto" style={{ width: '100%', borderRadius: '6px', marginBottom: '15px' }}>
                <h2 style={styles.basicDetailTitle}>用户信息</h2>
                <div style={styles.infoColumn}>
                  <h5 style={styles.infoColumnTitle}>基本信息</h5>
                  <Row wrap style={styles.infoItems}>
                    <Col xxs="24" l="12" style={styles.infoItem}>
                      <span style={styles.infoItemLabel}>用户昵称：</span>
                      <span style={styles.infoItemValue}>{data.name}</span>
                    </Col>
                    <Col xxs="24" l="12" style={styles.infoItem}>
                      <span style={styles.infoItemLabel}>用户编号：</span>
                      <span style={styles.infoItemValue}>{data.name}</span>
                    </Col>
                    <Col xxs="24" l="12" style={styles.infoItem}>
                      <span style={styles.infoItemLabel}>联系电话：</span>
                      <span style={styles.infoItemValue}>{data.phone} </span>
                    </Col>
                    <Col xxs="24" l="12" style={styles.infoItem}>
                      <span style={styles.infoItemLabel}>联系邮箱：</span>
                      <span style={styles.infoItemValue}>{data.email}</span>
                    </Col>
                    <Col xxs="24" l="12" style={styles.infoItem}>
                      <span style={styles.infoItemLabel}>联系地址：</span>
                      <span style={styles.infoItemValue}>{data.address}</span>
                    </Col>
                  </Row>
                </div>
              </Card>
              <MainData />
              <Card bodyHeight="auto" bordered={false} style={{ marginTop: '20px', marginBottom: '20px' }}>
                <Row gutter={16}>
                  <Col span={7}>
                    <div style={{ backgroundColor: 'rgb(246,246,246)', height: '200px' }}>
                      <img
                        style={{ padding: '80px 140px' }}
                        src="#"
                        alt=""
                      />
                    </div>
                  </Col>
                  <Col span={17}>
                    <Row gutter={8}>
                      <Col span={6}>
                        <div class={styles.img}>
                          <img src="http://112.74.168.74:8889/images/map/u78.png" alt="" />
                        </div>
                      </Col>
                      <Col span={6}>
                        <div class={styles.img}>
                          <img src="http://112.74.168.74:8889/images/map/u78.png" alt="" />
                        </div>
                      </Col>
                      <Col span={6}>
                        <div class={styles.img}>
                          <img src="http://112.74.168.74:8889/images/map/u78.png" alt="" />
                        </div>
                      </Col>
                      <Col span={6}>
                        <div class={styles.img}>
                          <img src="http://112.74.168.74:8889/images/map/u78.png" alt="" />
                        </div>
                      </Col>
                    </Row>
                    <Row>
                      <Col span={12} style={{ padding: '20px' }}>
                        <div className="bigNum">
                          <ul>
                            <li>宽:10cm</li>
                            <li>高:10cm</li>
                            <li>重:10cm</li>
                          </ul>
                        </div>
                      </Col>
                      <Col span={12} style={{ padding: '20px', paddingTop: '30px' }}>
                        郑州大学拥有一支优秀的物联网团队，兼物联网硬件设计，物联网平台搭建，物联网项目孵化等能力。雾霾盒子就是其中的一项作品，雾霾盒子以笔筒，盒子等多种形态存在
                    </Col>
                    </Row>
                  </Col>
                </Row>
              </Card>
              <Card style={{ width: '100%', borderRadius: '6px' }} bodyHeight="auto">
                <Row guntter={20} style={{ height: 450, marginBottom: 60 }}>
                  <Col span={16} style={{ backgroundColor: '#fff' }}>
                    <UserStatChart />
                  </Col>
                  <Col span={8} style={{ textAlign: 'center', }}>
                  <h1>实时Scada</h1>  
                    <img style={{paddingTop: '95%',height:'auto'}} src="http://112.74.168.74:8889/images/map/u108.png" alt=""/>
                  </Col>
                </Row>
              </Card>
              <Row guntter={20} style={{ height: 500, marginTop: '20px' }}>
                <Col l={12} s={24} xxs={24}>
                  <Card style={{ width: '98%', borderRadius: '6px' }} bodyHeight='auto'>
                    <ChartTypeLine />
                  </Card>
                </Col>
                <Col l={12} s={24} xxs={24}>
                  <Card style={{ width: '98%', float: 'right', borderRadius: '6px' }} bodyHeight='auto'>
                    <ChartTypeLine />
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      },
      {
        tab: "记录", key: "history", content:
          <div>
            <MainData />
            <Row guntter={20} style={{ height: 500, marginTop: '20px' }}>
              <Col l={12} s={24} xxs={24}>
                <Card style={{ width: '98%', borderRadius: '6px' }} bodyHeight='auto'>
                  <ChartTypeLine />
                </Card>
              </Col>
              <Col l={12} s={24} xxs={24}>
                <Card style={{ width: '98%', float: 'right', borderRadius: '6px' }} bodyHeight='auto'>
                  <ChartTypeLine />
                </Card>
              </Col>
            </Row>
          </div>
      },
      {
        tab: "详情", key: "info", content:
          <div>
            <Card bodyHeight="auto" style={{ width: '100%', borderRadius: '6px', marginBottom: '15px' }}>
              <div style={styles.infoColumn}>
                <Row wrap style={styles.infoItems}>
                  <Col xxs="24" l="12" style={styles.infoItem}>
                    <span style={styles.infoItemLabel}>用户昵称：</span>
                    <span style={styles.infoItemValue}>{data.name}</span>
                  </Col>
                  <Col xxs="24" l="12" style={styles.infoItem}>
                    <span style={styles.infoItemLabel}>用户编号：</span>
                    <span style={styles.infoItemValue}>{data.name}</span>
                  </Col>
                  <Col xxs="24" l="12" style={styles.infoItem}>
                    <span style={styles.infoItemLabel}>联系电话：</span>
                    <span style={styles.infoItemValue}>{data.phone} </span>
                  </Col>
                  <Col xxs="24" l="12" style={styles.infoItem}>
                    <span style={styles.infoItemLabel}>联系邮箱：</span>
                    <span style={styles.infoItemValue}>{data.email}</span>
                  </Col>
                  <Col xxs="24" l="12" style={styles.infoItem}>
                    <span style={styles.infoItemLabel}>联系地址：</span>
                    <span style={styles.infoItemValue}>{data.address}</span>
                  </Col>
                </Row>
              </div>
            </Card>
            <Card bordered={false} style={{ width: '100%', borderRadius: '6px', marginTop: '20px', textAlign: 'center' }}>
              <Row gutter={16}>
                <Col l={5} xxs={24}>
                  <ul>
                    <li style={styles.bigNum}>总异常次数</li>
                    <li style={styles.symbol}>0次</li>
                  </ul>
                </Col>
                <Col l={5} xxs={24}>
                  <ul>
                    <li style={styles.bigNum}>总异常次数</li>
                    <li style={styles.symbol}>0次</li>
                  </ul>
                </Col>
                <Col l={4} xxs={24}>
                  <ul>
                    <li style={styles.bigNum}>总数据</li>
                    <li style={styles.symbol}>1000次</li>
                  </ul>
                </Col>
                <Col l={5} xxs={24}>
                  <ul>
                    <li style={styles.bigNum}>总运行时长</li>
                    <li style={styles.symbol}>60小时</li>
                  </ul>
                </Col>
                <Col l={5} xxs={24}>
                  <ul>
                    <li style={styles.bigNum}>系统版本</li>
                    <li style={styles.symbol}>1.0.1</li>
                  </ul>
                </Col>
              </Row>
            </Card>
            <FeedList />
          </div>
      },
    ];
    return <div className="spec-detail-total-page">
      <Card style={{ width: '100%', borderRadius: '6px', marginBottom: '20px' }} bodyHeight='auto'>
        <h2 style={{ color: '#000', paddingLeft: 15, paddingTop: 10, fontWeight: 700 }}> <img width={30} src="http://112.74.168.74:8889/images/spec-detail-total/u2237.png" alt="" />  规格名称：雾霾盒子 <Button style={{float:'right'}} type="primary"><Link to='spec/detail' style={{ color: '#fff' }}>规格详情</Link></Button> </h2>
        <div  style={{ float: 'right' ,marginBotton:'15px'}} >
            <h2>状态：</h2> <li>  <Switch checkedChildren="开" unCheckedChildren="关" /> </li>
        </div>
        <div style={styles.infoColumn}>
          <Row wrap style={styles.infoItems}>
            <Col xxs="24" l="8" style={styles.infoItem}>
              <span style={styles.infoItemLabel}>添加时间：</span>
              <span style={styles.infoItemValue}>{data.time}</span>
            </Col>
            <Col xxs="24" l="8" style={styles.infoItem}>
              <span style={styles.infoItemLabel}>规格编号：</span>
              <span style={styles.infoItemValue}>{data.sid}</span>
            </Col>
            <Col xxs="24" l="8" style={styles.infoItem}>
              <span style={styles.infoItemLabel}>设备标识：</span>
              <span style={styles.infoItemValue}>{data.token} </span>
            </Col>
            <Col xxs="24" l="8" style={styles.infoItem}>
              <span style={styles.infoItemLabel}>绑定状态：</span>
              <span style={styles.infoItemValue}>{data.dstate}</span>
            </Col>
            <Col xxs="24" l="8" style={styles.infoItem}>
              <span style={styles.infoItemLabel}>设备分类：</span>
              <span style={styles.infoItemValue}>{data.type}</span>
            </Col>
            <Col xxs="24" l="8" style={styles.infoItem}>
              <span style={styles.infoItemLabel}>设备标签：</span>
              <span style={styles.infoItemValue}>{data.tag}</span>
            </Col>
            <Col xxs="24" l="8" style={styles.infoItem}>
              <span style={styles.infoItemLabel}>运行状态：</span>
              <span style={styles.infoItemValue}>{data.runstate}</span>
            </Col>
            <Col xxs="24" l="8" style={styles.infoItem}>
              <span style={styles.infoItemLabel}>设备备注：</span>
              <span style={styles.infoItemValue}>{data.note}</span>
            </Col>
          </Row>
        </div>
      </Card>
      <Tab style={{ background: '#fff', marginBottom: 30 }}>
        {tabs.map(item => (
          <TabPane key={item.key} tab={item.tab} >
            {item.content}
          </TabPane>
        ))}
      </Tab>
    </div>;
  }
}

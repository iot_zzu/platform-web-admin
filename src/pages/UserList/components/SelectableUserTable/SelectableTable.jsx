import React, { Component } from 'react';
import { Link } from 'react-router';
import { Table, Input, Button, Field, Icon, Pagination,Tag, Dropdown, Menu } from '@icedesign/base';
import IceContainer from '@icedesign/container';

const getMockData = () => {
  const result = [];
  for (let i = 0; i < 5; i++) {
    result.push({
      id: 100 + i,
      name:
        <ul>
          <li>吴加好<Tag size="small">标签</Tag></li>
          <li style={styles.subTitle}>请问你的梦想是什么</li>
        </ul>
      ,
      title:
        <ul>
          <li>设备数量</li>
          <li>3个</li>
        </ul>
      ,
      type:
        <ul>
          <li>2018-05-07 11:08</li>
          <li>2018-05-07 11:08</li>
        </ul>
      ,
      status:
        <ul>
          <li>河南省/郑州市/金水区</li>
        </ul>
      ,
      active:
      <span> <span style={{ color: 'green' }}>●</span>启用&nbsp;&nbsp;</span>
    ,
    });
  }
  return result;
};

// 注意：下载数据的功能，强烈推荐通过接口实现数据输出，并下载
// 因为这样可以有下载鉴权和日志记录，包括当前能不能下载，以及谁下载了什么

export default class SelectableUserTable extends Component {

  field = new Field(this);    // 实例创建


  static displayName = 'SelectableUserTable';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);

    // 表格可以勾选配置项
    this.rowSelection = {
      // 表格发生勾选状态变化时触发
      onChange: (ids) => {
        console.log('ids', ids);
        this.setState({
          selectedRowKeys: ids,
        });
      },
      // 全选表格时触发的回调
      onSelectAll: (selected, records) => {
        console.log('onSelectAll', selected, records);
      },
      // 支持针对特殊行进行定制
      getProps: (record) => {
        return {
          disabled: record.id === 100306660941,
        };
      },
    };

    this.state = {
      selectedRowKeys: [],
      dataSource: getMockData(),
    };
  }

  clearSelectedKeys = () => {
    this.setState({
      selectedRowKeys: [],
    });
  };

  deleteSelectedKeys = () => {
    const { selectedRowKeys } = this.state;
    console.log('delete keys', selectedRowKeys);
  };

  deleteItem = (record) => {
    const { id } = record;
    console.log('delete item', id);
  };

  renderOperator = (value, index, record) => {
    return (
      <div>
        <Link to="user/detail">详情&nbsp;</Link>
      </div>
    );
  };

  render() {
    const init = this.field.init;
    return (
      <div className="selectable-table" style={styles.SelectableUserTable}>
        <IceContainer style={styles.IceContainer}>
          <div>
            <a href="/" download>
              <Icon size="small" type="download" /> 导出表格数据到 .csv 文件
            </a>
          </div>
        </IceContainer>
        <IceContainer>
          <Table
            dataSource={this.state.dataSource}
            isLoading={this.state.isLoading}
            rowSelection={{
              ...this.rowSelection,
              selectedRowKeys: this.state.selectedRowKeys,
            }}
          >
            <Table.Column title="姓名" dataIndex="name" width={150} />
            <Table.Column title="设备" dataIndex="title" width={100} />
            <Table.Column title="注册时间" dataIndex="type" width={120} />
            <Table.Column title="地址" dataIndex="status" width={150} />
            <Table.Column title="状态" dataIndex="active" width={80} />
            <Table.Column
              title="操作"
              cell={this.renderOperator}
              lock="right"
              width={80}
            />
          </Table>
          <div style={styles.pagination}>
            <Pagination onChange={this.change} />
          </div>
        </IceContainer>
      </div>
    );
  }
}

const styles = {
  batchBtn: {
    marginRight: '10px',
  },
  IceContainer: {
    marginBottom: '20px',
    minHeight: 'auto',
    display: 'flex',
    justifyContent: 'space-between',
  },
  removeBtn: {
    marginLeft: 10,
  },
  pagination: {
    textAlign: 'right',
    paddingTop: '26px',
  },
  subTitle: {  
    fontSize: '12px',
    color: 'rgb(102,102,102)',
  },
};

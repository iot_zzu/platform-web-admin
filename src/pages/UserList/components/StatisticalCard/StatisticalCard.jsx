import React, { Component } from 'react';
import IceContainer from '@icedesign/container';
import { Grid ,Tag,Button} from '@icedesign/base';
import { div } from 'gl-matrix/src/gl-matrix/vec4';

const { Row, Col } = Grid;

const dataSource = [
  {
    text:'林东东',
    number:  <div style={{marginLeft:'15px',marginTop:'-15px'}}>
              <span style={{fontSize:13}}>15617817579&nbsp;&nbsp;&nbsp;&nbsp;15617817579@163.com</span>
              <p>这里应该填写个人简介这里应该填写个人简介这里应该填写个人简介，这里应该填写个人简介，这里应该填写个人简介，这里应该填写个人简介</p>        
            </div>,
    circle: {
      width: 36,
      height: 36,
      icon: 'https://gw.alicdn.com/tfs/TB1YDjNh4rI8KJjy0FpXXb5hVXa-36-31.png',
    },
    helpURL: 'http://taobao.com',
  },
  {
    text: '',
    number: <div style={{marginLeft:'50px'}}>
              <p>注册时间：<span>2016-12-12 12:12</span></p>
              <p>最近时间：<span>2018-12-12 12:12</span></p>
              <p>用户备注：<span>雾霾盒子是真的好</span></p>
            </div>,
    circle: {
      width: 0,
      height: 0,
     
    },
    
  },
  {
    text: '',
    number: <div style={{marginLeft:'50px'}}>
              <p>登录IP<span>192.168.128.56</span></p>
              <p>登录地址：<span>河南省/郑州市/金水区</span></p>
            </div>,
    circle: {
      width: 0,
      height: 0,
      icon: '',
    },
   
  },
  {
    text: '',
    number: <div><Tag>禁用</Tag><Tag>启用</Tag></div>,
    circle: {
      width: 0,
      height: 0,
      icon: '',
    },
    
  },
];

export default class StatisticalCard extends Component {
  static displayName = 'StatisticalCard';

  constructor(props) {
    super(props);
    this.state = {};
  }

  renderItem = () => {
    return dataSource.map((data, idx) => {
      const imgStyle = {
        width: `${data.circle.width}px`,
        height: `${data.circle.height}px`,
      };
      return (
        <Col
          xxs={24}
          xs={12}
          l={6}
          key={idx}
          style={styles.statisticalCardItem}
        >
          <div style={styles.circleWrap}>
            <img src={data.circle.icon} style={imgStyle} alt="" />
          </div>
          <div style={styles.statisticalCardDesc}>
            <div style={styles.statisticalCardText}>
              {data.text}
              <a href={data.helpURL} target="_blank">
                <img
                  src=""
                  style={styles.itemHelp}
                  alt=""
                />
              </a>
            </div>
            <div style={styles.statisticalCardNumber}>{data.number}</div>
          </div>
        </Col>
      );
    });
  };

  render() {
    return (
      <div className="statistical-card" style={styles.statisticalCard}>
        <IceContainer style={styles.statisticalCardItems}>
          <Row wrap style={{ width: '100%' ,height:'150px'}}>
            {this.renderItem()}
          </Row>
        </IceContainer>
      </div>
    );
  }
}

const styles = {
  statisticalCardItems: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: '20px',
  },
  statisticalCardItem: {
    display: 'flex',
    flexDirection: 'row',
    margin: '10px 30px',
  },
  circleWrap: {
    backgroundColor: '#FFECB3',
    width: '0px',
    height: '0px',
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '50%',
    marginRight: '10px',
  },
  statisticalCardDesc: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'top',
  },
  statisticalCardText: {
    position: 'relative',
    top:'-15px',
    color: '#333333',
    fontSize: '20px',
    fontWeight: 'bold',
    marginLeft: '20px',
  },
  statisticalCardNumber: {
    color: '#333333',
    fontSize: '12px',
  },
  itemHelp: {
    width: '12px',
    height: '12px',
    position: 'absolute',
    top: '1px',
    right: '-15px',
  },
};

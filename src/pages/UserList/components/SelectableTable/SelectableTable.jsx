import React, { Component } from 'react';
import { Link } from 'react-router';
import { Table, Input, Button, Field, Icon, Pagination, Dropdown, Menu } from '@icedesign/base';
import IceContainer from '@icedesign/container';

/* 
            <Table.Column title="编码" dataIndex="id" width={120} />
            <Table.Column title="类型" dataIndex="title" width={160} />
            <Table.Column title="模板" dataIndex="template" width={160} />
            <Table.Column title="发布状态" dataIndex="status" width={120} />
*/

const getMockData = () => {
  const result = [];
  for (let i = 0; i < 10; i++) {
    result.push({
      name:
        <ul>
          <li>智能灯</li>
          <li>简单的数据采集</li>
        </ul>
      ,
      title:
        <ul>
          <li>10个设备</li>
          <li>3个字段</li>
        </ul>
      ,
      type:
        <ul>
          <li>boxa-boxa-01</li>
          <li>2018-05-02</li>
        </ul>
      ,
      status:
        <ul>
          <li>spec-spec-spec-spec-spec</li>
          <li>生活/居家</li>
        </ul>
      ,
    });
  }
  return result;
};

// 注意：下载数据的功能，强烈推荐通过接口实现数据输出，并下载
// 因为这样可以有下载鉴权和日志记录，包括当前能不能下载，以及谁下载了什么

export default class SelectableTable extends Component {

  field = new Field(this);    // 实例创建


  static displayName = 'SelectableTable';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);

    // 表格可以勾选配置项
    this.rowSelection = {
      // 表格发生勾选状态变化时触发
      onChange: (ids) => {
        console.log('ids', ids);
        this.setState({
          selectedRowKeys: ids,
        });
      },
      // 全选表格时触发的回调
      onSelectAll: (selected, records) => {
        console.log('onSelectAll', selected, records);
      },
      // 支持针对特殊行进行定制
      getProps: (record) => {
        return {
          disabled: record.id === 100306660941,
        };
      },
    };

    this.state = {
      selectedRowKeys: [],
      dataSource: getMockData(),
    };
  }

  clearSelectedKeys = () => {
    this.setState({
      selectedRowKeys: [],
    });
  };

  deleteSelectedKeys = () => {
    const { selectedRowKeys } = this.state;
    console.log('delete keys', selectedRowKeys);
  };

  deleteItem = (record) => {
    const { id } = record;
    console.log('delete item', id);
  };

  renderOperator = (value, index, record) => {
    return (
      <div>
        <Link to="spec/detail">详情&nbsp;</Link>|
        <Dropdown trigger={<a href="#">&nbsp;更多<Icon type="arrow-down" /></a>} triggerType="click">
          <Menu>
            <Menu.Item><Link to="spec/edit"> 编辑</Link></Menu.Item>
            <Menu.Item>启用/禁用</Menu.Item>
            <Menu.Item>查看设备</Menu.Item>
            <Menu.Item>快速入口</Menu.Item>
          </Menu>
        </Dropdown>
      </div>
    );
  };

  render() {
    const init = this.field.init;
    return (
      <div className="selectable-table" style={styles.selectableTable}>
        <IceContainer style={styles.IceContainer}>
          <div>
            <Button size="small" style={styles.batchBtn}>
              <Link to="spec/add">
                增加
            </Link>
            </Button>
            <Button
              onClick={this.deleteSelectedKeys}
              size="small"
              style={styles.batchBtn}
              disabled={!this.state.selectedRowKeys.length}
            >
              <Icon type="ashbin" />删除
            </Button>
            <Button
              onClick={this.clearSelectedKeys}
              size="small"
              style={styles.batchBtn}
            >
              <Icon type="close" />清空选中
            </Button>
          </div>
          <div>
            <a href="/" download>
              <Icon size="small" type="download" /> 导出表格数据到 .csv 文件
            </a>
          </div>
        </IceContainer>
        <IceContainer>
          <Table
            dataSource={this.state.dataSource}
            isLoading={this.state.isLoading}
            rowSelection={{
              ...this.rowSelection,
              selectedRowKeys: this.state.selectedRowKeys,
            }}
          >
            <Table.Column title="编码" dataIndex="name" width={120} />
            <Table.Column title="类型" dataIndex="title" width={160} />
            <Table.Column title="模板" dataIndex="type" width={160} />
            <Table.Column title="发布状态" dataIndex="status" width={150} />
            <Table.Column
              title="操作"
              cell={this.renderOperator}
              lock="right"
              width={80}
            />
          </Table>
          <div style={styles.pagination}>
            <Pagination onChange={this.change} />
          </div>
        </IceContainer>
      </div>
    );
  }
}

const styles = {
  batchBtn: {
    marginRight: '10px',
  },
  IceContainer: {
    marginBottom: '20px',
    minHeight: 'auto',
    display: 'flex',
    justifyContent: 'space-between',
  },
  removeBtn: {
    marginLeft: 10,
  },
  pagination: {
    textAlign: 'right',
    paddingTop: '26px',
  },
};

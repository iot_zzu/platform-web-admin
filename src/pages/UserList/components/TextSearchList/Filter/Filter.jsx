import React, { Component } from 'react';

import IceContainer from '@icedesign/container';
import { Button } from '@icedesign/base';

export default class Filter extends Component {
  render() {
    return (
      <IceContainer style={styles.container}>
        <div style={styles.category}>
          <span style={styles.label}>一级分类：</span>
          <span style={styles.item}><Button type='primary'>全部</Button></span>
          <span style={styles.item}>类目一</span>
          <span style={styles.item}>类目二</span>
          <span style={styles.item}>类目三</span>
          <span style={styles.item}>类目四</span>
          <span style={styles.item}>类目五</span>
          <span style={styles.item}>类目六</span>
          <span style={styles.item}>类目七</span>
          <span style={styles.item}>类目八</span>
        </div>
        <div style={styles.others}>
          <span style={styles.label}>二级分类：</span>
          <span style={styles.item}><Button type='primary'>全部</Button></span>
          <span style={styles.item}>类目一</span>
          <span style={styles.item}>类目二</span>
          <span style={styles.item}>类目三</span>
          <span style={styles.item}>类目四</span>
          <span style={styles.item}>类目五</span>
          <span style={styles.item}>类目六</span>
          <span style={styles.item}>类目七</span>
          <span style={styles.item}>类目八</span>
        </div>
      </IceContainer>
    );
  }
}

const styles = {
  container: {},
  category: {
    padding: '0 10px 15px',
    borderBottom: '1px solid #eee',
  },
  others: {
    padding: '15px 10px 0',
  },
  label: {
    color: '#333',
    fontSize: '14px',
    marginRight: '10px',
  },
  item: {
    marginRight: '10px',
  },
};

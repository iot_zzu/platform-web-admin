

import React, { Component } from 'react';
import TimeFilterTable from './components/TimeFilterTable';
import StatisticalCard from './components/StatisticalCard';
import TabTable from './components/TabTable';
import SimpleSlide from './components/SimpleSlider';
import MainData from './components/MainData';
import CustomBreadcrumb from '../../components/CustomBreadcrumb';
import SelectableTable from './components/SelectableTable'
import { Tab ,Grid} from "@icedesign/base";
import './UserList.scss';
import '@icedesign/data-binder';
import TextSearchList from './components/TextSearchList';
import SelectableUserTable from './components/SelectableUserTable/SelectableTable';
export default class UserList extends Component {
  static displayName = 'UserList';

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const breadcrumb = [
      { text: '用户管理', link: '' },
      { text: '添加列表', link: '#/user/list' },
    ];
    return (
      <div className="create-user-page">
        <CustomBreadcrumb dataSource={breadcrumb} />
        <TextSearchList/>
        <SelectableUserTable/>
      </div>
    );
  }
}

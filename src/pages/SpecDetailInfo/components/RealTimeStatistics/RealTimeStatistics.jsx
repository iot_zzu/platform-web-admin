import React, { Component } from 'react';
import { Grid } from '@icedesign/base';

const { Row, Col } = Grid;

export default class RealTimeStatistics extends Component {
  static displayName = 'RealTimeStatistics';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="real-time-statistics" style={{height:'200px'}}>
        <Row gutter="0" style={styles.items}>
          <Col xxs="24" s="12" l="6">
            <div style={{ ...styles.itemBody, ...styles.green }}>
              <img  style={{ ...styles.itemimge }}src="https://img.alicdn.com/tfs/TB1zHh_dQyWBuNjy0FpXXassXXa-1350-900.jpg_400x200q90.jpg" alt="图片"/>
            </div>
          </Col>
          <Col xxs="24" s="12" l="6">
            <div style={{ ...styles.itemBody, ...styles.green }}>
              <img src="https://img.alicdn.com/tfs/TB1zHh_dQyWBuNjy0FpXXassXXa-1350-900.jpg_400x200q90.jpg" alt="图片"/>
            </div>
          </Col>
          <Col xxs="24" s="12" l="6">
            <div style={{ ...styles.itemBody, ...styles.green }}>
              <img src="https://img.alicdn.com/tfs/TB1zHh_dQyWBuNjy0FpXXassXXa-1350-900.jpg_400x200q90.jpg" alt="图片"/>
            </div>
          </Col>
          <Col xxs="24" s="12" l="6">
            <div style={{ ...styles.itemBody, ...styles.green }}>
              <img src="https://img.alicdn.com/tfs/TB1zHh_dQyWBuNjy0FpXXassXXa-1350-900.jpg_400x200q90.jpg" alt="图片"/>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

const styles = {
  distance:{
    marginTop:'30px'
  },
  item: {
    marginBottom: '20px',
  },
  itemBody: {
    marginBottom: '20px',
    padding: '15px',
    borderRadius: '4px',
    color: '#fff',
    width: '250px',
    height: '158px',
  },
  itemimage:{
    width: '100%',
  },
  itemRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  itemTitle: {
    position: 'relative',
  },
  titleText: {
    margin: 0,
    fontSize: '14px',
  },
  tag: {
    position: 'absolute',
    right: 0,
    top: 0,
    padding: '2px 4px',
    borderRadius: '4px',
    fontSize: '12px',
    background: 'rgba(255, 255, 255, 0.3)',
  },
  itemNum: {
    margin: '16px 0',
    fontSize: '32px',
  },
  total: {
    margin: 0,
    fontSize: '12px',
  },
  desc: {
    margin: 0,
    fontSize: '12px',
  },
  green: {
    background: '#fff',
  },
  lightBlue: {
    background: '#fff',
  },
  darkBlue: {
    background: '#fff',
  },
  navyBlue: {
    background: '#fff',
  },
};

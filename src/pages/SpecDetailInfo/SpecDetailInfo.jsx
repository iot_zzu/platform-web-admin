import React, { Component } from 'react';
import BasicException from './components/BasicException';
import RealTimeStatistics from './components/RealTimeStatistics';
import ComplexProgressTable from './components/ComplexProgressTable';
import { Grid,Button,Card } from '@icedesign/base';
import { IceContainer } from '@icedesign/container';
const { Row, Col } = Grid;
function handleChange(key) {
  console.log("change", key);
}

function handleClick(key) {
  console.log("click", key);
}
export default class SpecDetailInfo extends Component {
  static displayName = 'SpecDetailInfo';

  constructor(props) {
    super(props);
    this.state = {};
  }
  
  
  
  render() {
    return (
      <div className="spec-detail-info-page">
        <div style={{ marginTop: '10px',textAlign:'center'}}>
        <Card bodyHeight='auto'>
        <RealTimeStatistics />
        </Card>
        </div>  
        <Row gutter="16" style={{marginTop: '50px',height: 500,marginBottom:30}}>
          <Col  span={10} style={{background: '#fff',marginRight:10,padding:15,borderRadius:8}}>
            <div style={{background:'#E9E9E9',width:'100%',height:'100%'}}>
              <img style={{width:'100%',height: '100%'}} src="https://img.alicdn.com/tfs/TB1zHh_dQyWBuNjy0FpXXassXXa-1350-900.jpg_400x200q90.jpg" alt="未知图片"/>
            </div>
          </Col>
          <Col  span={14} style={{background: '#fff',borderRadius:8,padding:'0 25px'}}>
            <h2 style={{borderBottom:'1px solid #E9E9E9',fontWeight:600}}>规格信息</h2>
            <dt>属性</dt> 
            <dd>待测量与发布</dd> 
            <dt>描述</dt> 
            <dd>郑州大学拥有一支优秀的物联网团队，兼物联网硬件设计，物联网平台搭建，物联网项目孵化等能力。雾霾盒子就是其中的一项作品，雾霾盒子以笔筒，盒子等多种形态存在</dd> 
          </Col>
        </Row>
        < ComplexProgressTable/> 
      </div>
    );
  }
}

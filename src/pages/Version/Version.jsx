

import React, { Component } from 'react';
import CustomBreadcrumb from '../../components/CustomBreadcrumb';
import TabTable from './components/TabTable';
import AddDialog from './components/AddDialog';
import EditDialog from './components/EditDialog';
import ColumnForm from './components/ColumnForm';
import SelectableTable from './components/SelectableTable'

import './Version.scss';
import { Card, Grid } from '@icedesign/base';
const { Row, Col } = Grid;

export default class Version extends Component {
  static displayName = 'Version';

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const breadcrumb = [
      { text: '版本管理', link: '' },
      { text: '版本列表', link: '#/version/list' },
    ];
    return (
      <div className="tag-list-page">
        <CustomBreadcrumb dataSource={breadcrumb} />
        <Row>
          <Col span={24}>
            <ColumnForm />
          </Col>
        </Row> 
        <Row>
          <SelectableTable />
        </Row>
      </div>         
    );
  }
}

import React, { Component } from 'react';
import { Dialog, Grid, Input, Radio, Button, Upload  } from '@icedesign/base';
import IceContainer from '@icedesign/container';
import {
  FormBinderWrapper as IceFormBinderWrapper,
  FormBinder as IceFormBinder,
  FormError as IceFormError,
} from '@icedesign/form-binder';
import { enquireScreen } from 'enquire-js';

const { Row, Col } = Grid;
const { Group: RadioGroup } = Radio;
const { ImageUpload } = Upload;

const defaultValue = {
  productName: '',
  uniCode: '',
  des: '',
};

export default class SimpleFormDialog extends Component {
  static displayName = 'SimpleFormDialog';

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      value: defaultValue,
      isMobile: false,
    };
  }

  componentDidMount() {
    this.enquireScreenRegister();
  }

  enquireScreenRegister = () => {
    const mediaCondition = 'only screen and (max-width: 720px)';

    enquireScreen((mobile) => {
      this.setState({
        isMobile: mobile,
      });
    }, mediaCondition);
  };

  showDialog = () => {
    this.setState({
      visible: true,
    });
  };

  hideDialog = () => {
    this.setState({
      visible: false,
    });
  };

  onOk = () => {
    this.refForm.validateAll((error) => {
      if (error) {
        // show validate error
        return;
      }
      // deal with value

      this.hideDialog();
    });
  };

  onFormChange = (value) => {
    this.setState({
      value,
    });
  };

  render() {
    const { isMobile } = this.state;
    const simpleFormDialog = {
      ...styles.simpleFormDialog,
    };
    // 响应式处理
    if (isMobile) {
      simpleFormDialog.width = '320px';
    }

    return (
      <div>
        <Dialog
          className="Editdialog"
          style={simpleFormDialog}
          autoFocus={false}
          footerAlign="center"
          title="添加/编辑分类"
          {...this.props}
          onOk={this.onOk}
          onCancel={this.hideDialog}
          onClose={this.hideDialog}
          isFullScreen
          visible={this.state.visible}
        >
          <IceFormBinderWrapper
            ref={(ref) => {
              this.refForm = ref;
            }}
            value={this.state.value}
            onChange={this.onFormChange}
          >
            <div style={styles.dialogContent}>
            <Row style={styles.formRow} align="center">
              <Col span="5" style={{ height: "100px", lineHeight: "100px" }}>
                <label style={styles.formLabel}>产品icon：</label>
              </Col>
              <Col>
                <ImageUpload
                  listType="picture-card"
                  action="//next-upload.shuttle.alibaba.net/upload" // 该接口仅作测试使用，业务请勿使用
                  accept="image/png, image/jpg, image/jpeg, image/gif, image/bmp"
                  locale={{
                    image: {
                      cancel: "取消上传",
                      addPhoto: "上传图片"
                    }
                  }}
                  defaultFileList={[
                    {
                      name: "IMG.png",
                      status: "done",
                      downloadURL:
                        "https://img.alicdn.com/tfs/TB1zHh_dQyWBuNjy0FpXXassXXa-1350-900.jpg",
                      fileURL:
                        "https://img.alicdn.com/tfs/TB1zHh_dQyWBuNjy0FpXXassXXa-1350-900.jpg",
                      imgURL:
                        "https://img.alicdn.com/tfs/TB1zHh_dQyWBuNjy0FpXXassXXa-1350-900.jpg"
                    }
                  ]}
                />
              </Col>
            </Row>
              <Row style={styles.formRow}>
                <Col span={`${isMobile ? '10' : '4'}`}>
                  <label >产品名称：</label>
                </Col>
                <Col span={`${isMobile ? '18' : '13'}`}>
                  <IceFormBinder
                    required
                    min={2}
                    max={15}
                    message="当前字段必填，且最少 2 个字最多 15 个字"
                  >
                    <Input
                      name="productName"
                      style={styles.input}
                      rows={5}
                    />
                  </IceFormBinder>
                  <IceFormError name="productName" />
                </Col>
              </Row>
              <Row style={styles.formRow}>         
                <Col span={`${isMobile ? '10' : '4'}`}>
                  <label style={styles.formLabel}>唯一码：</label>
                </Col>
                <Col>
                  <IceFormBinder>
                    <Input
                      name="uniCode"
                      style={styles.input}
                      rows={5}
                    />
                  </IceFormBinder>
                </Col>
              </Row>
              <Row style={styles.formRow}>       
                <Col span={`${isMobile ? '10' : '4'}`}>
                  <label style={styles.formLabel}>产品描述：</label>
                </Col>
                <Col>
                  <IceFormBinder>
                    <Input
                      name="des"
                      style={styles.input}
                      multiple
                      placeholder="请输入详细内容"
                      rows={6}
                    />
                  </IceFormBinder>
                </Col>
              </Row>
            </div>
          </IceFormBinderWrapper>
        </Dialog>
        <Button onClick={this.showDialog} style={styles.add} type="primary">
          编辑
        </Button>
        </div>
    );
     
    
  }
}

const styles = {
  simpleFormDialog: { width: '640px'},
  dialogContent: { padding:'0 30px 10px 30px'},
  formRow: { marginTop: 20 },
  input: { width: '100%',marginTop: '8px' },
  inputt: {width: '100%', marginRight: '10px'},
  formLabel: { lineHeight: '26px', paddingRight: '15px' ,paddingTop: '50px', width: '25px'},
  add: {width: '100px'}
};

import React, { Component } from 'react';
import { Dialog, Grid, Input, Radio, Button } from '@icedesign/base';
import IceContainer from '@icedesign/container';
import {
  FormBinderWrapper as IceFormBinderWrapper,
  FormBinder as IceFormBinder,
  FormError as IceFormError,
} from '@icedesign/form-binder';
import { enquireScreen } from 'enquire-js';

const { Row, Col } = Grid;
const { Group: RadioGroup } = Radio;

const defaultValue = {
  version1: '',
  version2: '',
  version3: '',
  address: '',
  content: '',
};

export default class SimpleFormDialog extends Component {
  static displayName = 'SimpleFormDialog';

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      value: defaultValue,
      isMobile: false,
    };
  }

  componentDidMount() {
    this.enquireScreenRegister();
  }

  enquireScreenRegister = () => {
    const mediaCondition = 'only screen and (max-width: 720px)';

    enquireScreen((mobile) => {
      this.setState({
        isMobile: mobile,
      });
    }, mediaCondition);
  };

  showDialog = () => {
    this.setState({
      visible: true,
    });
  };

  hideDialog = () => {
    this.setState({
      visible: false,
    });
  };

  onOk = () => {
    this.refForm.validateAll((error) => {
      if (error) {
        // show validate error
        return;
      }
      // deal with value

      this.hideDialog();
    });
  };

  onFormChange = (value) => {
    this.setState({
      value,
    });
  };

  render() {
    const { isMobile } = this.state;
    const simpleFormDialog = {
      ...styles.simpleFormDialog,
    };
    // 响应式处理
    if (isMobile) {
      simpleFormDialog.width = '320px';
    }

    return (
      <div>
        <Dialog
          className="Editdialog"
          style={simpleFormDialog}
          autoFocus={false}
          footerAlign="center"
          title="编辑/添加版本"
          {...this.props}
          onOk={this.onOk}
          onCancel={this.hideDialog}
          onClose={this.hideDialog}
          isFullScreen
          visible={this.state.visible}
        >
          <IceFormBinderWrapper
            ref={(ref) => {
              this.refForm = ref;
            }}
            value={this.state.value}
            onChange={this.onFormChange}
          >
            <div style={styles.dialogContent}>
            <Row style={styles.formRow}>
                <Col span={`${isMobile ? '6' : '3'}`}>
                  <label style={styles.formLabel}>版本：</label>
                </Col>
                  <Col>
                  <IceFormBinder
                      required
                      min={1}
                      max={1}
                      message="当前字段必填，且为1个数字"
                    >
                      <Input
                        name="version1"
                        style={styles.inputt}
                      />
                    </IceFormBinder>
                    <IceFormError name="version1" />
                  </Col>
                  <Col>
                    <IceFormBinder
                      required
                      min={1}
                      max={1}
                      message="当前字段必填，且为1个数字"
                    >
                      <Input
                        name="version2"
                        style={styles.inputt}
                      />
                    </IceFormBinder>
                    <IceFormError name="version2" />
                  </Col>
                  <Col>
                    <IceFormBinder
                      required
                      min={1}
                      max={1}
                      message="当前字段必填，且为1个数字"
                    >
                      <Input
                        name="version3"
                        style={styles.inputt}
                      />
                    </IceFormBinder>
                    <IceFormError name="version3" />
                  </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col span={`${isMobile ? '6' : '3'}`}>
                  <label style={styles.formLabel}>地址：</label>
                </Col>
                <Col span={`${isMobile ? '18' : '16'}`}>
                  <IceFormBinder
                    required
                    min={2}
                    max={20}
                    message="当前字段必填，且最少 2 个字最多 10 个字"
                  >
                    <Input
                      name="address"
                      style={styles.input}
                    />
                  </IceFormBinder>
                  <IceFormError name="address" />
                </Col>
              </Row>

              <Row style={styles.formRow}>
                
                <Col span={`${isMobile ? '6' : '3'}`}>
                  <label style={styles.formLabel}>描述：</label>
                </Col>
                <Col>
                  <IceFormBinder>
                    <Input
                      name="content"
                      style={styles.input}
                      multiple
                      placeholder="请输入详细内容"
                      rows={6}
                    />
                  </IceFormBinder>
                </Col>
              </Row>
            </div>
          </IceFormBinderWrapper>
        </Dialog>
        <Button type="secondary" onClick={this.showDialog}>
          编辑
        </Button>
      </div>
    );
  }
}

const styles = {
  simpleFormDialog: { width: '640px'},
  dialogContent: { padding:'0 30px 10px 30px'},
  formRow: { marginTop: 20 },
  input: { width: '100%' },
  inputt: {width: '100%', marginRight: '10px'},
  formLabel: { lineHeight: '26px', paddingRight: '10px' },
};

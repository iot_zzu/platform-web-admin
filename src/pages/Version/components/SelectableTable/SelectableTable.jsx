import React, { Component } from 'react';
import { Table, Button, Icon, Pagination, Grid } from '@icedesign/base';
import IceContainer from '@icedesign/container';
import AddDialog from '../AddDialog'
import EditDialog from '../EditDialog'
import Edit from '../Edit'
import HiddenTable from '../HiddenTable'

const { Row, Col } = Grid;

const getMockData = () => {
  const result = [];
  for (let i = 0; i < 10; i++) {
    result.push({
      id: 1003066 + i,
      title: {
        name: `name`,
      },
      type: 'demo示例',
      uniCode: '111-111-11' + i,
      productDes: '这是一个产品描述',
      publisher: 'User',
      rate: '5',
      time1: '2000-01-20' ,
      time2: '2000-01-20' ,
      des: <HiddenTable />
        // <div>
        //   <Row style={styles.editDes}>
        //     <Col>版本号</Col>
        //     <Col>描述</Col>
        //     <Col>地址</Col>
        //     <Col>日期</Col>
        //     <Col></Col>
        //   </Row>
        //   <Row style={styles.editDes}>
        //     <Col>1.0.0</Col>
        //     <Col>这是描述</Col>
        //     <Col>http://www.baidu.com</Col>
        //     <Col>2018-12-12</Col>

        //     <Col><EditDialog /></Col>
        //   </Row>
        //   <Row style={styles.editDes}>
        //     <Col >1.0.1</Col>
        //     <Col >这是描述</Col>
        //     <Col >http://www.baidu.com</Col>
        //     <Col >2018-12-12</Col>
        //     <Col><EditDialog /></Col>
        //   </Row>
        // </div>, 
    });
  }
  return result;
};

// 注意：下载数据的功能，强烈推荐通过接口实现数据输出，并下载
// 因为这样可以有下载鉴权和日志记录，包括当前能不能下载，以及谁下载了什么

export default class SelectableTable extends Component {
  static displayName = 'SelectableTable';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      dataSource: getMockData(),

    };
  }

  clearSelectedKeys = () => {
    this.setState({
      selectedRowKeys: [],
    });
  };

  deleteSelectedKeys = () => {
    const { selectedRowKeys } = this.state;
    console.log('delete keys', selectedRowKeys);
  };

  deleteItem = (record) => {
    const { id } = record;
    console.log('delete item', id);
  };

  renderOperator = (value, index, record) => {
    return (
      <div>
        <Row>
          <Col><a><Edit /></a></Col>
        </Row>
      </div>
    );
  };

  render() {
    return (
      <div className="selectable-table" style={styles.selectableTable}>
        <IceContainer>
        <AddDialog />
          <Table
            dataSource={this.state.dataSource}
            isLoading={this.state.isLoading}
            isZebra={this.state.isZebra}
            hasBorder={false}
            onSort={this.onSort.bind(this)}

            expandedRowRender={record => record.des}

            onRowClick={() => console.log('rowClick')}
            onExpandedRowClick={() =>
              console.log('expandedRowClick')}
            expandedRowIndent={this.state.expandedRowIndent}
          >
            <Table.Column title="编码" dataIndex="id" sortable align="center" width={50} />
            <Table.Column title="名称" dataIndex="title.name" align="center" width={40} />
            {/* <Table.Column title="类型" dataIndex="type" width={160} /> */}
            <Table.Column title="唯一码" dataIndex="uniCode" align="center" width={60} />
            <Table.Column title="产品描述" dataIndex="productDes" align="center" width={100} />
            <Table.Column title="版本数量" dataIndex="rate" align="center" width={40} />
            <Table.Column title="操作者" dataIndex="publisher" align="center" width={35} />
            <Table.Column title="起始时间" dataIndex="time1" align="center" width={70} />
            <Table.Column title="结束时间" dataIndex="time2" align="center" width={70} />
            {/* <Table.Column title="产品描述" dataIndex="des" width={120} hight={40} /> */}
            <Table.Column
              title="操作"
              cell={this.renderOperator}
              // lock="right"
              width={80}
            />
          </Table>
          <div style={styles.pagination}>
            <Pagination onChange={this.change} />
          </div>
        </IceContainer>
      </div>
    );
  }

  onSort(dataIndex, order) {
    let dataSource = this.state.dataSource.sort(function(a, b) {
      let result = a[dataIndex] - b[dataIndex];
      return order === "asc" ? (result > 0 ? 1 : -1) : result > 0 ? -1 : 1;
    });
    this.setState({
      dataSource
    });
  }

  toggleCol() {
    this.setState({
      hasExpandedRowCtrl: false
    });
  }
}

const styles = {
  batchBtn: {
    marginRight: '10px',
  },
  IceContainer: {
    marginBottom: '20px',
    minHeight: 'auto',
    display: 'flex',
    justifyContent: 'space-between',
  },
  removeBtn: {
    marginLeft: 10,
  },
  pagination: {
    textAlign: 'right',
    paddingTop: '26px',
  },
  editDes: {
    height: '30px',
    lineheight: '50px',
    marginLeft: '20px',
    background: '#F4F4F4',
    width: '100%',
    paddingLeft: '20px',
    align: "center",
  }
};

import React, { Component } from 'react';
import IceContainer from '@icedesign/container';
import { Grid, Badge, Card, Icon } from '@icedesign/base';
import './LatestNews.scss';

const { Row, Col } = Grid;

const dataSource = {
  articles: [
    {
      title: '这里是文章的标题1',
      time: '2018-03-31',
    },
    {
      title: '这里是文章的标题2',
      time: '2018-02-02',
    },
    {
      title: '这里是文章的标题3',
      time: '2018-01-22',
    },
    {
      title: '这里是文章的标题4',
      time: '2018-02-02',
    },
    {
      title: '这里是文章的标题5',
      time: '2018-01-22',
    },
    {
      title: '这里是文章的标题6',
      time: '2018-02-02',
    },
  ],
  comments: [
    {
      title: '这里是最新的评论1',
      time: '2018-02-26',
      num: '18',
    },
    {
      title: '这里是最新的评论2',
      time: '2018-01-22',
      num: '22',
    },
    {
      title: '这里是最新的评论3',
      time: '2018-01-18',
      num: '36',
    },
    {
      title: '这里是最新的评论4',
      time: '2018-01-18',
      num: '29',
    },
  ],
};

export default class LatestNews extends Component {
  static displayName = 'LatestNews';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="latest-news" style={styles.container}>
        <Row wrap gutter="20">
          <Col xxs="24" s="16" l="16">
            <Card bodyHeight='auto' style={{width:'100%', borderRadius:'6px'}}>
              <h1 style={{paddingBottom:'75px'}}>设备分布</h1>
              <img style={{ width: '100%' }} src="https://gw.alipayobjects.com/zos/rmsportal/HBWnDEUXCnGnGrRfrpKa.png" alt="" />
            </Card>
          </Col>
          <Col xxs="24" s="8" l="8">
            <Card bodyHeight="auto" style={{ width: '100%', borderRadius: '6px' }}>
              <Row>
                <Col span={24}>
                  <div className='sort'>
                    <div style={{ fontSize: '16' }}>
                      <b>设备排行</b>
                      <span style={{ float: 'right' }}>
                        <Icon type="info-circle-o" />
                      </span>
                    </div>
                    <ul style={{ paddingTop: '20px' }}>
                      <li>
                        <Badge
                          count={1}
                          style={{
                            backgroundColor: 'rgb(240,65,52)',
                            color: '#fff',
                            boxShadow: '0 0 0 1px #d9d9d9 inset',
                          }}
                        />
                        <span className='text'>
                          <b>雾霾盒子</b>
                        </span>
                        <span style={{ float: 'right' }}>443,234</span>
                      </li>
                      <li>
                        <Badge
                          count={2}
                          style={{
                            backgroundColor: 'rgb(245,106,0)',
                            color: '#fff',
                            boxShadow: '0 0 0 1px #d9d9d9 inset',
                          }}
                        />
                        <span className='text'>
                          <b>智能衣柜</b>
                        </span>
                        <span style={{ float: 'right' }}>417,454</span>
                      </li>
                      <li>
                        <Badge
                          count={3}
                          style={{
                            backgroundColor: 'rgb(0,162,174)',
                            color: '#fff',
                            boxShadow: '0 0 0 1px #d9d9d9 inset',
                          }}
                        />
                        <span className='text'>
                          <b>智能花盆</b>
                        </span>
                        <span style={{ float: 'right' }}>415,274</span>
                      </li>
                      <li>
                        <Badge
                          count={4}
                          style={{
                            backgroundColor: 'rgb(240,242,245)',
                            color: '#000',
                            boxShadow: '0 0 0 1px #d9d9d9 inset',
                          }}
                        />
                        <span className='text'>智能椅子</span>
                        <span style={{ float: 'right' }}>393,683</span>
                      </li>
                      <li>
                        <Badge
                          count={5}
                          style={{
                            backgroundColor: 'rgb(240,242,245)',
                            color: '#000',
                            boxShadow: '0 0 0 1px #d9d9d9 inset',
                          }}
                        />
                        <span className='text'>智能窗户</span>
                        <span style={{ float: 'right' }}>353,394</span>
                      </li>
                      <li>
                        <Badge
                          count={6}
                          style={{
                            backgroundColor: 'rgb(240,242,245)',
                            color: '#000',
                            boxShadow: '0 0 0 1px #d9d9d9 inset',
                          }}
                        />
                        <span className='text'>智能桌子</span>
                        <span style={{ float: 'right' }}>347,231</span>
                      </li>
                      <li>
                        <Badge
                          count={7}
                          style={{
                            backgroundColor: 'rgb(240,242,245)',
                            color: '#000',
                            boxShadow: '0 0 0 1px #d9d9d9 inset',
                          }}
                        />
                        <span className='text'>智能门</span>
                        <span style={{ float: 'right' }}>343,232</span>
                      </li>
                      <li>
                        <Badge
                          count={8}
                          style={{
                            backgroundColor: 'rgb(240,242,245)',
                            color: '#000',
                            boxShadow: '0 0 0 1px #d9d9d9 inset',
                          }}
                        />
                        <span className='text'>智能笔</span>
                        <span style={{ float: 'right' }}>333,232</span>
                      </li>
                      <li>
                        <Badge
                          count={9}
                          style={{
                            backgroundColor: 'rgb(240,242,245)',
                            color: '#000',
                            boxShadow: '0 0 0 1px #d9d9d9 inset',
                          }}
                        />
                        <span className='text'>智能床</span>
                        <span style={{ float: 'right' }}>323,232</span>
                      </li>
                    </ul>
                  </div>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

const styles = {
  cardContainer: {
    height: '286px',
    overflowY: 'auto',
  },
  cardTitle: {
    position: 'relative',
    margin: '0 0 10px',
    fontSize: '16px',
    fontWeight: 'bold',
    color: '#333',
  },
  more: {
    position: 'absolute',
    right: 0,
    fontSize: '12px',
    color: '#666',
  },
  item: {
    position: 'relative',
    display: 'block',
  },
  itemTime: {
    position: 'absolute',
    right: 0,
    top: 6,
    fontSize: '12px',
  },
  itemTitle: {
    height: '34px',
    lineHeight: '34px',
    fontSize: '13px',
  },
  itemComment: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    marginBottom: '10px',
  },
  commentTitle: {
    height: '28px',
    lineHeight: '28px',
    fontSize: '13px',
  },
  commentTime: {
    fontSize: '12px',
  },
  commentNum: {
    position: 'absolute',
    right: 0,
    top: 6,
    width: '24px',
    height: '24px',
    lineHeight: '24px',
    fontSize: '12px',
    textAlign: 'center',
    borderRadius: '50px',
    background: '#FF2851',
    color: '#fff',
  },
};

import React, { Component } from 'react';
import SimpleFluencyForm from './components/SimpleFluencyForm';
import CustomBreadcrumb from '../../components/CustomBreadcrumb';
export default class SpecificationEdit extends Component {
  static displayName = 'SpecificationEdit';

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const breadcrumb = [
      { text: '规格管理', link: '' },
      { text: '修改规格', link: '#/spec/edit' },
    ];
    return (
      <div className="specification-edit-page">
       <CustomBreadcrumb dataSource={breadcrumb} />
        <SimpleFluencyForm />
      </div>
    );
  }
}

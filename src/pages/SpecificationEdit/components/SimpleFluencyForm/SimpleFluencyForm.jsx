import React, { Component } from 'react';
import IceContainer from '@icedesign/container';
import { Link } from 'react-router';
import { Step, Grid, Input, Button, Upload, Tag, CascaderSelect, Dialog, Icon, Switch } from '@icedesign/base';
import {
  FormBinderWrapper,
  FormBinder,
  FormError,
} from '@icedesign/form-binder';
const { Row, Col } = Grid;
const { DragUpload } = Upload;
const { ImageUpload } = Upload;
const telPattern = /^(1[\d]{1}[\d]{9})|(((400)-(\d{3})-(\d{4}))|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)$|^([ ]?)$/;

export default class SimpleFluencyForm extends Component {
  static displayName = 'SimpleFluencyForm';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      step: 0,
      formValue: {
        username: '',
        email: '',
        phone: '',
        address: '',
      },
    };
  }

  // ICE: React Component 的生命周期

  componentWillMount() { }

  componentDidMount() { }

  componentWillReceiveProps() { }

  componentWillUnmount() { }

  formChange = (newValue) => {
    this.setState({
      formValue: newValue,
    });
  };

  nextStep = () => {
    this.setState({ step: this.state.step + 1 });
  };

  renderStep = (step) => {
    if (step === 0) {
      const { username, email, phone, address } = this.state.formValue;
      const initValue = {
        username,
        email,
        phone,
        address,
      };
      return (
        <IceContainer style={styles.form}>
          <FormBinderWrapper
            ref={(form) => {
              this.form = form;
            }}
          >
            <div>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  规格分类：生活/雾霾盒子
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  规格名称：
                </Col>
                <Col s="14" l="12">
                  <FormBinder type="text">
                    <Input
                      name="name"
                      size="large"
                      style={{ width: '100%' }}
                    />
                  </FormBinder>
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  规格icon：
                </Col>
                <Col s="14" l="12">
                  <DragUpload
                    action="//next-upload.shuttle.alibaba.net/upload" // 该接口仅作测试使用，业务请勿使用
                    accept="image/png, image/jpg, image/jpeg, image/gif, image/bmp"
                  />
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  规格描述：
                </Col>
                <Col s="14" l="12">
                  <Input multiple placeholder="multiple" />
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  规格备注：
                </Col>
                <Col s="14" l="12">
                  <Input multiple placeholder="multiple" />
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  更多图片：
                </Col>
                <Col s="14" l="12">
                  <ImageUpload
                    listType="picture-card"
                    action="//next-upload.shuttle.alibaba.net/upload" // 该接口仅作测试使用，业务请勿使用
                    accept="image/png, image/jpg, image/jpeg, image/gif, image/bmp"
                    locale={{
                      image: {
                        cancel: "取消上传",
                        addPhoto: "上传图片"
                      }
                    }}
                    defaultFileList={[
                      {
                        name: "IMG.png",
                        status: "done",
                        downloadURL:
                          "https://img.alicdn.com/tps/TB19O79MVXXXXcZXVXXXXXXXXXX-1024-1024.jpg",
                        fileURL:
                          "https://img.alicdn.com/tps/TB19O79MVXXXXcZXVXXXXXXXXXX-1024-1024.jpg",
                        imgURL:
                          "https://img.alicdn.com/tps/TB19O79MVXXXXcZXVXXXXXXXXXX-1024-1024.jpg"
                      }
                    ]}
                  />
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  标签（选填）
                </Col>
                <Col s="14" l="12">
                  <div>
                    <Tag shape="deletable">Tag1</Tag>
                    <Tag shape="deletable" animation={false}>
                      Tag2
                    </Tag>
                  </div>
                </Col>
              </Row>
              <Row>

                <Col offset={7}>
                  <Button onClick={this.nextStep} type="primary">
                    下一步
                  </Button>
                </Col>
              </Row>
            </div>

          </FormBinderWrapper>

        </IceContainer>
      );
    } else if (step === 1) {
      return (
        <IceContainer style={styles.form}>
          <FormBinderWrapper
            ref={(form) => {
              this.form = form;
            }}
          >
            <div>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  规格分类：生活/雾霾盒子/盒子一
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  标识前缀：
                </Col>
                <Col s="14" l="12">
                  <FormBinder type="text">
                    <Input
                      name="name"
                      size="large"
                      style={{ width: '100%' }}
                    />
                  </FormBinder>
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  scada模型：
                </Col>
                <Col s="14" l="12">
                  <DragUpload
                    action="//next-upload.shuttle.alibaba.net/upload" // 该接口仅作测试使用，业务请勿使用
                    accept="image/png, image/jpg, image/jpeg, image/gif, image/bmp"
                  />
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  状态：
                </Col>
                <Col s="14" l="12">
                  <Switch checkedChildren="开" unCheckedChildren="关" />
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  属性：
                </Col>
                <Col s="14" l="12">
                  <FormBinder type="text">
                    <Input
                      name="prop1"
                      size="large"
                      style={{ width: '30%' }}
                    />
                  </FormBinder>
                  --
                  <FormBinder type="text">
                    <Input
                      name="prop2"
                      size="large"
                      style={{ width: '65%' }}
                    />
                  </FormBinder>
                </Col>
              </Row>
              <Row>

                <Col offset={7}>
                  <Link to="spec/list">
                    <Button onClick={this.nextStep} type="primary">
                      完成
                  </Button>
                  </Link>
                </Col>
              </Row>
            </div>
          </FormBinderWrapper>
        </IceContainer>
      );
    }
  };

  render() {
    return (
      <div className="simple-fluency-form">
        <IceContainer>
          <Step current={this.state.step} type="dot">
            <Step.Item key={0} title="基本信息" />
            <Step.Item key={1} title="高级信息" />
          </Step>
        </IceContainer>
        {this.renderStep(this.state.step)}
      </div>
    );
  }
}

const styles = {
  form: {
    padding: '40px 20px',
  },
  formLabel: {
    textAlign: 'right',
    lineHeight: '1.7rem',
    paddingRight: '10px',
  },
  formRow: {
    marginBottom: '20px',
  },
  formErrorWrapper: {
    marginTop: '5px',
  },
  simpleFluencyForm: {},
};

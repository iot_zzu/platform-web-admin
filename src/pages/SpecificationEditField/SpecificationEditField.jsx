

import React, { Component } from 'react';
import CustomBreadcrumb from '../../components/CustomBreadcrumb';
import ComplexProgressTable from './components/ComplexProgressTable';
import {  Table,Upload, Button ,Grid} from "@icedesign/base";
const { Row, Col } = Grid;
const { Core } = Upload;
export default class SpecificationEditField extends Component {
  constructor(props) {
    super(props);

    this.state = {
      disabled: false,
      dragable: false
    };

    /* eslint-disable */
    ["onDisabledHandler", "onDragableHandler", "onAbortHandler"].map(fn => {
      this[fn] = this[fn].bind(this);
    });
    /* eslint-enable */
  }

  onDisabledHandler() {
    this.setState({
      disabled: !this.state.disabled
    });
  }

  onDragableHandler() {
    this.setState({
      dragable: !this.state.dragable
    });
  }

  onAbortHandler() {
    this.refs.inner.abort();
  }

  static displayName = 'SpecificationEditField';
// ////////////



  


  render() {
    const breadcrumb = [
      { text: '字段管理', link: '' },
      { text: '修改字段', link: '#/cate/list' },
    ];
    return (
      <div className="cate-list-page">
          <CustomBreadcrumb dataSource={breadcrumb} />
          <Row style={{backgroundColor:'#fff',height:200,padding:10}}>
            <ul style={{height:'50%'}}>
              <li style={{fontSize:25,fontWeight:600}}>修改字段</li>
              <li style={{marginTop:20}}>设备规格基本信息</li>
            </ul>
            
          </Row>
          <div style={{width: '100%',borderTop:'1px solid #E9E9E9',background:'#fff',height:155}}>
                <Core
              ref="inner"
              style={{
                display: "block",
                textAlign: "center",
                width: "700px",
                height: "50px",
                margin:'auto',
                marginTop: 55,
                lineHeight: "100%",
                textAlign: "center",
                border: "1px dashed #aaa",
                borderRadius: "5px",
                fontSize: "12px"
              }}
              action="//next-upload.shuttle.alibaba.net/upload" // 该接口仅作测试使用，业务请勿使用
              accept="image/png, image/jpg, image/jpeg, image/gif, image/bmp"
              name="filename"
              disabled={this.state.disabled}
              multiple
              dragable={this.state.dragable}
              multipart={{ _token: "sdj23da" }}
              headers={{ Authorization: "user_1" }}
              beforeUpload={beforeUpload}
              onStart={onStart}
              onProgress={onProgress}
              onSuccess={onSuccess}
              onError={onError}
              onAbort={onAbort}
            >
              <span style={{display:'block',marginTop:16,fontSize: 18,color:'#E9e9e9'}}>+添加</span>
              {this.state.disabled
                ? "禁止上传"
                : this.state.dragable ? "支持点击或者拖拽上传" : ""}
            </Core>
            </div>
        <br />
        <div>
        </div>
          < ComplexProgressTable/>  
      </div>
    );
  }
}

function beforeUpload(file) {
  console.log("beforeUpload callback : ", file);
}

function onStart(files) {
  console.log("onStart callback : ", files);
}

function onProgress(e, file) {
  console.log("onProgress callback : ", e, file);
}

function onSuccess(res, file) {
  console.log("onSuccess callback : ", res, file);
}

function onError(err, res, file) {
  console.log("onError callback : ", err, res, file);
}

function onAbort(e, file) {
  console.log("onAbort callback : ", e, file);
}



import React, { Component } from 'react';
import CustomBreadcrumb from '../../components/CustomBreadcrumb';
import TabTable from './components/TabTable';
import './Devices.scss';
import { Card, Grid } from '@icedesign/base';
import TextSearchList from './components/TextSearchList/index';
import SelectableTable from './components/SelectableTable/SelectableTable';
const { Row, Col } = Grid;
export default class CateList extends Component {
  static displayName = 'Devices';

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const breadcrumb = [
      { text: '设备管理', link: '' },
      { text: '设备列表', link: '#/device/list' },
    ];
    return (
      <div>
      <CustomBreadcrumb dataSource={breadcrumb} />
        <TextSearchList />
        <SelectableTable />
        
    </div>
    );
  }
}

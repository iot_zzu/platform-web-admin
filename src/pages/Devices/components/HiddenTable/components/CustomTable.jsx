import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table } from '@icedesign/base';

const getMockData = () => {
  const result = [];
  for (let i = 0; i < 10; i++) {
    result.push({
      version: '1.0.' + i,
      address: 'http://www.baidu.com',
      description: '这是描述',
      date: '2016 - 02 - 15'
    });
  }
  return result;
};

export default class CustomTable extends Component {
  static displayName = 'CustomTable';

  static propTypes = {
    dataSource: PropTypes.array,
    columns: PropTypes.array.isRequired,
  };

  static defaultProps = {
    dataSource: getMockData(),
  };

  constructor(props) {
    super(props);
    this.state = {
      // dataSource: getMockData(),
    };
  }

  renderColumns = () => {
    const { columns } = this.props;
    return columns.map((item) => {
      if (typeof item.render === 'function') {
        return (
          <Table.Column
            title={item.title}
            key={item.key}
            cell={item.render}
            width={item.width || 150}
          />
        );
      }

      return (
        <Table.Column
          key={item.key}
          title={item.title}
          dataIndex={item.dataIndex}
          width={item.width || 150}
        />
      );
    });
  };

  render() {
    return <Table {...this.props}>{this.renderColumns()}</Table>;
  }
}

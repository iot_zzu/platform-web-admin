import React, { Component } from 'react';
import IceContainer from '@icedesign/container';
import { Tab,Card  } from '@icedesign/base';
import axios from 'axios';
import CustomTable from './components/CustomTable';
import EditDialog from './components/EditDialog';
import DeleteBalloon from './components/DeleteBalloon';
import TextCard from './../TextCard/index';
import RealTimeStatistics from './../RealTimeStatistics/index';


export default class TabTable extends Component {
  static displayName = 'TabTable';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render() {
    const { dataSource } = this.state;
    return (
      <div className="tab-table">
        <Card bodyHeight='auto' style={{ width: '100%',background:'rgb(240,242,245)'}}>
          <TextCard />
          <RealTimeStatistics />
        </Card>
      </div>
    );
  }
}

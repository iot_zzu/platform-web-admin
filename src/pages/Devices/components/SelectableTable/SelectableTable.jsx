import React, { Component } from 'react';
import { Link } from 'react-router';
import { Table, Button, Icon,Tag, Pagination, Grid } from '@icedesign/base';
import IceContainer from '@icedesign/container';
import HiddenTable from '../HiddenTable'

const { Row, Col } = Grid;
const getMockData = () => {
  const result = [];
  for (let i = 0; i < 6; i++) {
    result.push({
      id: (
        1000+i
      ),
      name: (
        <Row>
          <Col span={6}>
            <img src="http://112.74.168.74:8889/images/specification/u1187.png" alt="icon" />
          </Col>
          <Col span={18}>
            <ul>
              <li>
                <b>
                  智能灯&nbsp;<Tag size="small">标签</Tag>
                </b>
              </li>
              <li>简单的数据采集</li>
            </ul>
          </Col>
        </Row>
      ),
      attr: (
        <ul>
          <li>温度:10</li>
          <li>湿度:20</li>
          <li>PM2.5:12</li>
        </ul>
      ),
      spec: (
        <ul>
          <li>spec-spec-spec-spec-spec-1</li>
          <li>boxa-boxa-01-00000001</li>
          <li>2018-04-29 2:12</li>
          <l1>河南/郑州</l1>
        </ul>
      ),
      user: (
        <ul>
          <li>2018-04-29 2:12</li>
          <li>楚博豪</li>
          <li>1866686868</li>
          <l1>9432738@qq.com</l1>
        </ul>
      ),
      rate: (
        <div>
          <li>
            <span style={{ color: 'blue' }}>●</span>&nbsp;允许
          </li>
          <li>
            <span style={{ color: 'green' }}>●</span>&nbsp;运行
          </li>
          <li>
            <span style={{ color: 'orange' }}>●</span>&nbsp;异常
          </li>
        </div>
      ),
      des: <HiddenTable />
    });
  }
  return result;
};

// 注意：下载数据的功能，强烈推荐通过接口实现数据输出，并下载
// 因为这样可以有下载鉴权和日志记录，包括当前能不能下载，以及谁下载了什么

export default class SelectableTable extends Component {
  static displayName = 'SelectableTable';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);

    this.state = {
      selectedRowKeys: [],
      dataSource: getMockData(),

    };
  }

  clearSelectedKeys = () => {
    this.setState({
      selectedRowKeys: [],
    });
  };

  deleteSelectedKeys = () => {
    const { selectedRowKeys } = this.state;
    console.log('delete keys', selectedRowKeys);
  };

  deleteItem = (record) => {
    const { id } = record;
    console.log('delete item', id);
  };

  renderOperator = (value, index, record) => {
    return (
      <div>
        <Row>
          <Link to="device/detail">详情</Link>
        </Row>
      </div>
    );
  };

  render() {
    return (
      <div className="selectable-table" style={styles.selectableTable}>
        <IceContainer>
          <Table
            dataSource={this.state.dataSource}
            isLoading={this.state.isLoading}
            isZebra={this.state.isZebra}
            hasBorder={false}
            onSort={this.onSort.bind(this)}

            expandedRowRender={record => record.des}

            onRowClick={() => console.log('rowClick')}
            onExpandedRowClick={() =>
              console.log('expandedRowClick')}
            expandedRowIndent={this.state.expandedRowIndent}
          >
            <Table.Column title="编号" dataIndex="id" sortable align="left" width={40} />  
            <Table.Column title="名称" dataIndex="name" align="left" width={140} />
            <Table.Column title="参数" dataIndex="attr" align="left" width={60} />
            <Table.Column title="规格" dataIndex="spec" align="left" width={140} />
            <Table.Column title="用户" dataIndex="user" align="left" width={100} />
            <Table.Column title="状态" dataIndex="rate" align="left" width={40} />
            <Table.Column
              title="操作"
              cell={this.renderOperator}
              // lock="right"
              width={80}
            />
          </Table>
          <div style={styles.pagination}>
            <Pagination onChange={this.change} />
          </div>
        </IceContainer>
      </div>
    );
  }

  onSort(dataIndex, order) {
    let dataSource = this.state.dataSource.sort(function(a, b) {
      let result = a[dataIndex] - b[dataIndex];
      return order === "asc" ? (result > 0 ? 1 : -1) : result > 0 ? -1 : 1;
    });
    this.setState({
      dataSource
    });
  }

  toggleCol() {
    this.setState({
      hasExpandedRowCtrl: false
    });
  }
}

const styles = {
  batchBtn: {
    marginRight: '10px',
  },
  IceContainer: {
    marginBottom: '20px',
    minHeight: 'auto',
    display: 'flex',
    justifyContent: 'space-between',
  },
  removeBtn: {
    marginLeft: 10,
  },
  pagination: {
    textAlign: 'right',
    paddingTop: '26px',
  },
  editDes: {
    height: '30px',
    lineheight: '50px',
    marginLeft: '20px',
    background: '#F4F4F4',
    width: '100%',
    paddingLeft: '20px',
    align: "center",
  }
};

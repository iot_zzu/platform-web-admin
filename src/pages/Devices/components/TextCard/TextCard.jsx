import React, { Component } from 'react';
import IceContainer from '@icedesign/container';
import { Grid, Card } from '@icedesign/base';
import './TextCard.scss';

const { Row, Col } = Grid;

export default class TextCard extends Component {
  static displayName = 'TextCard';

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <IceContainer>
        <Row wrap justify="space-between" style={{ width: '100%', background: 'rgb(240,242,245)', }}>
          <Col xxs="12" s="12" l="5" style={styles.textCardItem}>
            <div style={styles.textCardSubtitle}>温度</div>
            <div className="text-card-title" style={styles.textCardTitle}>
              <img style={{ padding: '11px', width: '100%' }} src="http://112.74.168.74:8889/images/map/u108.png" alt="" />
            </div>
          </Col>
          <Col xxs="12" s="12" l="5" style={styles.textCardItem}>
            <div style={styles.textCardSubtitle}>湿度</div>
            <div className="text-card-title" style={styles.textCardTitle}>
              <img style={{ padding: '11px', width: '100%' }} src="http://112.74.168.74:8889/images/map/u108.png" alt="" />
            </div>
          </Col>
          <Col xxs="12" s="12" l="5" style={styles.textCardItem}>
            <div style={styles.textCardSubtitle}>PM2.5</div>
            <div className="text-card-title" style={styles.textCardTitle}>
              <img style={{ padding: '11px', width: '100%' }} src="http://112.74.168.74:8889/images/monitor/u284.png" alt="" />
            </div>
          </Col>
          <Col xxs="12" s="12" l="5" style={styles.textCardItem}>
            <div style={styles.textCardSubtitle}>pm10</div>
            <div className="text-card-title" style={styles.textCardTitle}>
              <img style={{ padding: '11px', width: '100%' }} src="http://112.74.168.74:8889/images/monitor/u284.png" alt="" />
            </div>
          </Col>

        </Row>

      </IceContainer>
    );
  }
}

const styles = {
  textCardItem: {
    // borderRight: '1px solid #F0F0F0',
    width: '30%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'left',
    alignItems: 'left',
    background: '#fff',
    marginTop: '15px',
    margin: '15px'
  },
  textCardSubtitle: {
    padding: '25px',
    fontSize: '12px',
  },
  textCardTitle: {
    fontSize: '16px',
  },
  textCardNumber: {
    fontSize: '24px',
    fontWeight: 'bold',
  },
};

import React, { Component } from 'react';
import IceContainer from '@icedesign/container';
import ArticleList from './ArticleList';
import Filter from './Filter';
import {
  Form,
  Input,
  Switch,
  Search,
  Grid,
  Button,
  Select,
  Tag,
  Icon,
  DatePicker,
  Balloon,
  Field
} from "@icedesign/base";
const { Row, Col } = Grid;
const { RangePicker } = DatePicker;


const FormItem = Form.Item;
const style = {
  padding: "20px",
  background: "#fff",
  marginBottom: "20px",
  borderRadius: '6px'
};
const formItemLayout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 }
};
const formItemLayout2 = {
  labelCol: { span: 4 },
  wrapperCol: { span: 12 }
};


export default class TextSearchList extends Component {
  field = new Field(this);
  static displayName = 'TextSearchList';

  render() {
    const init = this.field.init;
    return (
      <div className="text-search-list">
        <Filter />
        <Form
          style={style}
        >
          <Row >
            <Col span={8}>
              <FormItem {...formItemLayout} label='设备标识：'>
                <Input placeholder="请输入设备标识" />
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem {...formItemLayout} label='设备描述：' >
                <Input placeholder="请输入设备描述" />
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem {...formItemLayout} label='设备状态：' >
                <Select >
                  <div value="on">开启</div>
                  <div value="off">关闭</div>
                </Select>
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={8}>
              <FormItem
                label="添加日期："
                labelCol={{ span: 8 }}
              >
                <DatePicker.RangePicker />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem {...formItemLayout2} label='标签：' >
                <div>
                  <Tag shape="deletable" size="small">Tag1</Tag>
                  <Tag shape="deletable" size="small">Tag1</Tag>
                </div>
              </FormItem>
            </Col>
          </Row>
          <Row >
            <Col span={8}>
              <FormItem {...formItemLayout} label='规格名称：'>
                <Input placeholder="请输入规格名称" />
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem {...formItemLayout} label='规格备注：' >
                <Input placeholder="请输入设备描述" />
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem {...formItemLayout} label='运行状态：' >
                <Select >
                  <div value="on">开启</div>
                  <div value="off">关闭</div>
                </Select>
              </FormItem>
            </Col>
          </Row>
          <Row >
            <Col span={8}>
              <FormItem {...formItemLayout} label='客户账号：'>
                <Input placeholder="请输入客户账号" />
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem {...formItemLayout} label='客户邮箱：' >
                <Input placeholder="请输入客户邮箱" />
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem {...formItemLayout} label='设备位置：' >
                <Select >
                  <div value="location1">位置1</div>
                  <div value="location2">位置2</div>
                </Select>
              </FormItem>
            </Col>
          </Row>
          <Row >
            <Col span={8}>
              <FormItem {...formItemLayout} label='客户电话：'>
                <Input placeholder="请输入客户电话" />
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem
                label="绑定时间："
                labelCol={{ span: 8 }}
              >
                <DatePicker.RangePicker />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col style={{ textAlign: "right" }}>
              <Button type="primary" style={{ marginRight: "5px" }}>
                搜索
              </Button>
              <Button>清除条件</Button>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
}



import React, { Component } from 'react';
import CustomBreadcrumb from '../../components/CustomBreadcrumb';
import { Tree, Search,Table,Grid } from "@icedesign/base";
const { Row, Col } = Grid;
const { Node: TreeNode } = Tree;
const data = [
  {
    id: "生活",
    children: [
      {
        id: "B级菜单1",
        children: [
          {
            id: "居家",
            children: [
              {
                id: "雾霾"
              }
            ]
          },
          {
            id: "建议"
          }
        ]
      },
      {
        id: "B级菜单2",
        children: [
          {
            id: "雾霾"
          },
          {
            id: "C级菜单"
          }
        ]
      }
    ]
  }
];
////

const onChange = function(...args) {
  console.log(...args);
},
getData = () => {
  let result = [];
  for (let i = 0; i < 5; i++) {
    result.push({
      title: {
        name: `box1`
      },
      id: '这是雾霾盒子',
      time: 2000 + i
    });
  }
  return result;
},
render = (value, index, record) => {
  return <a>Remove({record.id})</a>;
},
rowSelection = {
  onChange: onChange,
  getProps: record => {
    return {
      disabled: record.id === 1003
    };
  }
};
/////
export default class SpecificationClassify extends Component {
  static displayName = 'SpecificationClassify';

  constructor(props) {
    super(props);
    this.state = {
      value: "",
      expandedKeys: ["0-0"],
      autoExpandParent: true
    };
    this.matchedKeys = [];
    this.handleSearch = this.handleSearch.bind(this);
    this.handleExpand = this.handleExpand.bind(this);
  }
  handleSearch(result) {
    const value = result.key;
    const matchedKeys = [];
    const loop = data =>
      data.forEach(item => {
        if (item.id.indexOf(value.trim()) > -1) {
          matchedKeys.push(item.id);
        }
        if (item.children && item.children.length) {
          loop(item.children);
        }
      });
    loop(data);

    this.setState({
      value: result.key,
      expandedKeys: matchedKeys,
      autoExpandParent: true
    });
    this.matchedKeys = matchedKeys;
  }

  handleExpand(keys) {
    this.setState({
      expandedKeys: keys,
      autoExpandParent: false
    });
  }

  
  
  render() {
    const breadcrumb = [
      { text: '分类管理', link: '' },
      { text: '添加分类', link: '#/spec/classify' },
    ];
    ///
    const loop = data =>
      data.map(item => {
        return (
          <TreeNode label={item.id} key={item.id}>
            {item.children && loop(item.children)}
          </TreeNode>
        );
      });

    const { value, expandedKeys, autoExpandParent } = this.state;
    const filterTreeNode = node =>
      value && this.matchedKeys.indexOf(node.props.eventKey) > -1;
      ///
    return (
      <div className="create-cate-page">
        <CustomBreadcrumb dataSource={breadcrumb} />
       <div className="demo-title"></div>
                <Row style={{height:450,backgroundColor:'#fff',padding:20}}>
                  <Col span="11" style={{borderRight:'2px solid  #E9E9E9 '}}>
                  <div>
                      <Search
                        type="normal"
                        size="medium"
                        searchText=""
                        value={value}
                        onSearch={this.handleSearch}
                      />
                      <Tree
                        expandedKeys={expandedKeys}
                        autoExpandParent={autoExpandParent}
                        filterTreeNode={filterTreeNode}
                        onExpand={this.handleExpand}
                      >
                      {loop(data)}
                       </Tree>
                  </div>
            </Col>
            <Col span="11" style={{paddingTop:5,paddingLeft:75}}>
              <Search type="normal" inputWidth={300} placeholder="请输入" />
              <Table dataSource={getData()} rowSelection={rowSelection}>
                <Table.Column title="描述" dataIndex="id"  style={{height:55}}/>
                <Table.Column title="名称" dataIndex="title.name" />
                <Table.Column title="时间" dataIndex="time" />
              </Table>
            </Col>
          </Row>
      </div>
    );
  }
}

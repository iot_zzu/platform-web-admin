import React, { Component } from 'react';
import CustomBreadcrumb from '../../components/CustomBreadcrumb';
import TabTable from './components/TabTable';
import TextSearchList from './components/TextSearchList';
import './SpecificationList.scss';
import Addification from './../AddSpecification/index';
import { Card, Grid } from '@icedesign/base';
import SelectableTable from './components/SelectableTable';
const { Row, Col } = Grid;

export default class SpecificationList extends Component {
  static displayName = 'SpecificationList';

  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const breadcrumb = [
      { text: '规格管理', link: '' },
      { text: '规格列表', link: '#/spec/list' },
    ];
    return (
      <div className="post-list-page">
        <CustomBreadcrumb dataSource={breadcrumb} />
        <Row>
          <Col span={24}>
            <TextSearchList />
            <SelectableTable/>  
          </Col>
        </Row>
      </div>
    );
  }
}

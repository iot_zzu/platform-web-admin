/* eslint  react/no-string-refs: 0 */
import React, { Component } from 'react';
import IceContainer from '@icedesign/container';
import { Input, Button, Radio,Select, Switch, Upload, Grid } from '@icedesign/base';
import {
  FormBinderWrapper as IceFormBinderWrapper,
  FormBinder as IceFormBinder,
  FormError as IceFormError,
} from '@icedesign/form-binder';
import './SettingsForm.scss';

const { Row, Col } = Grid;
const { Group: RadioGroup } = Radio;
const { ImageUpload } = Upload;

function beforeUpload(info) {
  console.log('beforeUpload callback : ', info);
}

function onChange(info) {
  console.log('onChane callback : ', info);
}

function onSuccess(res, file) {
  console.log('onSuccess callback : ', res, file);
}

function onError(file) {
  console.log('onError callback : ', file);
}

export default class SettingsForm extends Component {
  static displayName = 'SettingsForm';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      value: {
        name: '',
        token: '',
        unit: '请选择',
        desc: '',
        img: '',
        githubUrl: '',
        twitterUrl: '',
        description: '',
      },
    };
  }

  onDragOver = () => {
    console.log('dragover callback');
  };

  onDrop = (fileList) => {
    console.log('drop callback : ', fileList);
  };

  formChange = (value) => {
    console.log('value', value);
    this.setState({
      value,
    });
  };

  validateAllFormField = () => {
    this.refs.form.validateAll((errors, values) => {
      console.log('errors', errors, 'values', values);
    });
  };

  render() {
    return (
      <div className="settings-form">
        <IceContainer>
          <IceFormBinderWrapper
            value={this.state.value}
            onChange={this.formChange}
            ref="form"
          >
            <div style={styles.formContent}>
              <h2 style={styles.formTitle}>基本设置</h2>

              <Row style={styles.formItem}>
                <Col xxs="6" s="6" l="4" style={styles.label}>
                  名称：
                </Col>
                <Col s="18" l="16">
                  <IceFormBinder name="name" required max={10} message="必填">
                    <Input size="large" placeholder="请输入" />
                  </IceFormBinder>
                  <IceFormError name="name" />
                </Col>
              </Row>
              <Row style={styles.formItem}>
                <Col xxs="6" s="6" l="4" style={styles.label}>
                  标识：
                </Col>
                <Col s="18" l="16">
                  <IceFormBinder name="token" required max={10} message="必填">
                    <Input size="large" placeholder="请输入" />
                  </IceFormBinder>
                  <IceFormError name="token" />
                </Col>
              </Row>

              <Row style={styles.formItem}>
                <Col xxs="6" s="3" l="4" style={styles.label}>
                  单位：
                </Col>
                <Col s="12" l="10">
                  <IceFormBinder  name="unit">
                    <Select style={{ width: 150 }}
                    >
                      <Option value="common" disabled>
                        常用
                      </Option>
                      <Option value="temp">温度</Option>
                      <Option value="weight" disabled>
                        重量
                      </Option>
                      <Option value="t">吨</Option>
                    </Select>
                  </IceFormBinder>
                  <IceFormError name="unit" />
                </Col>
              </Row>
              <Row style={styles.formItem}>
                <Col xxs="6" s="3" l="4" style={styles.label}>
                  图形：
                </Col>
                <Col s="12" l="10">
                  <IceFormBinder  name="img">
                    <Select style={{ width: 150 }}
                    >
                      <Option value="common2" disabled>
                        常用
                      </Option>
                      <Option value="link">折线</Option>
                      <Option value="others" disabled>
                        其他
                      </Option>
                      <Option value="g">分布图</Option>
                    </Select>
                  </IceFormBinder>
                  <IceFormError name="img" />
                </Col>
              </Row>


              <Row style={styles.formItem}>
                <Col xxs="6" s="3" l="4" style={styles.label}>
                  描述：
                </Col>
                <Col s="12" l="16">
                  <IceFormBinder name="desc">
                    <Input size="large" multiple placeholder="请输入描述..." />
                  </IceFormBinder>
                  <IceFormError name="desc" />
                </Col>
              </Row>
            </div>
          </IceFormBinderWrapper>

          <Row style={{ marginTop: 20 }}>
            <Col offset="3">
              <Button
                size="large"
                type="primary"
                style={{ width: 100 }}
                onClick={this.validateAllFormField}
              >
                提 交
              </Button>
            </Col>
          </Row>
        </IceContainer>
      </div>
    );
  }
}

const styles = {
  label: {
    textAlign: 'right',
  },
  formContent: {
    width: '100%',
    position: 'relative',
  },
  formItem: {
    alignItems: 'center',
    marginBottom: 25,
  },
  formTitle: {
    margin: '0 0 20px',
    paddingBottom: '10px',
    borderBottom: '1px solid #eee',
  },
};

import React, { Component } from 'react';
import { Link } from 'react-router';
import { Table, Input, Button, Field, Icon, Pagination, Dropdown, Menu } from '@icedesign/base';
import IceContainer from '@icedesign/container';
import { Img } from '@icedesign/img';

/* 
            <Table.Column title="编码" dataIndex="id" width={120} />
            <Table.Column title="类型" dataIndex="title" width={160} />
            <Table.Column title="模板" dataIndex="template" width={160} />
            <Table.Column title="发布状态" dataIndex="status" width={120} />
*/

const getMockData = () => {
  const result = [];
  for (let i = 0; i < 5; i++) {
    result.push({
      name:
        <ul>
          <li><b>折线图</b></li>
          <li style={styles.subTitle}>展示密集型数据的红色折线图</li>
        </ul>
      ,
      desc:
        <ul>
          <li></li>
          <li style={styles.subTitle}>室内温度</li>
        </ul>
      ,
      type:
        <div>
          <img src="http://112.74.168.74:8889/images/map/u108.png" alt="模型"/>
        </div>
      ,
      field:
        <ul style={styles.subTitle}>
          <li>温度:0℃</li>
          <li>temperature</li>
        </ul>
      ,
    });
  }
  return result;
};

// 注意：下载数据的功能，强烈推荐通过接口实现数据输出，并下载
// 因为这样可以有下载鉴权和日志记录，包括当前能不能下载，以及谁下载了什么

export default class SelectableTable extends Component {

  field = new Field(this);    // 实例创建


  static displayName = 'SelectableTable';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);

    // 表格可以勾选配置项
    this.rowSelection = {
      // 表格发生勾选状态变化时触发
      onChange: (ids) => {
        console.log('ids', ids);
        this.setState({
          selectedRowKeys: ids,
        });
      },
      // 全选表格时触发的回调
      onSelectAll: (selected, records) => {
        console.log('onSelectAll', selected, records);
      },
      // 支持针对特殊行进行定制
      getProps: (record) => {
        return {
          disabled: record.id === 100306660941,
        };
      },
    };

    this.state = {
      selectedRowKeys: [],
      dataSource: getMockData(),
    };
  }

  clearSelectedKeys = () => {
    this.setState({
      selectedRowKeys: [],
    });
  };

  deleteSelectedKeys = () => {
    const { selectedRowKeys } = this.state;
    console.log('delete keys', selectedRowKeys);
  };

  deleteItem = (record) => {
    const { id } = record;
    console.log('delete item', id);
  };

  renderOperator = (value, index, record) => {
    return (
      <div>
        <Link to="/">删除&nbsp;</Link>|
        <Link to="spec/editfield" >&nbsp;编辑</Link>
      </div>
    );
  };

  render() {
    const init = this.field.init;
    return (
      <div className="selectable-table" style={styles.selectableTable}>
        <IceContainer>
          <Table hasHeader="false" isZebra="true" hasBorder="false"
            dataSource={this.state.dataSource}
            isLoading={this.state.isLoading}
            rowSelection={{
              ...this.rowSelection,
              selectedRowKeys: this.state.selectedRowKeys,
            }}
          >
            <Table.Column title="名称" dataIndex="name" width={120} />
            <Table.Column title="描述" dataIndex="desc" width={160} />
            <Table.Column title="模型图" dataIndex="type" width={160} />
            <Table.Column title="动态字段" dataIndex="field" width={150} />
            <Table.Column
              title="操作"
              cell={this.renderOperator}
              lock="right"
              width={80}
            />
          </Table>
          <div style={styles.pagination}>
            <Pagination onChange={this.change} />
          </div>
        </IceContainer>
      </div>
    );
  }
}

const styles = {
  subTitle:{
    color: 'rgb(141,141,141)',
    fontSize: '12px',
  },
  batchBtn: {
    marginRight: '10px',
  },
  IceContainer: {
    marginBottom: '20px',
    minHeight: 'auto',
    display: 'flex',
    justifyContent: 'space-between',
  },
  removeBtn: {
    marginLeft: 10,
  },
  pagination: {
    textAlign: 'right',
    paddingTop: '26px',
  },
};

import React, { Component } from 'react';
import IceContainer from '@icedesign/container';
import { Step, Grid, Input, Button, Upload, Tag, CascaderSelect, Dialog, Icon, Switch } from '@icedesign/base';
import {
  FormBinderWrapper,
  FormBinder,
  FormError,
} from '@icedesign/form-binder';
import SelectableTable from './../SelectableTable/SelectableTable';
import SettingsForm from './../SettingsForm/index';
const { Row, Col } = Grid;
const telPattern = /^(1[\d]{1}[\d]{9})|(((400)-(\d{3})-(\d{4}))|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)$|^([ ]?)$/;
const { DragUpload } = Upload;
const { ImageUpload } = Upload;
const dataSource = [
  {
    value: "2973",
    label: "家具",
    children: [
      {
        value: "2974",
        label: "雾霾盒子",
        children: [
          { value: "2975", label: "雾霾盒子V1" },
          { value: "2976", label: "雾霾盒子V2" }
        ]
      },
      {
        value: "",
        label: "智能衣柜",
        children: [
          { value: "2981", label: "智能衣柜V1" },
          { value: "2982", label: "智能衣柜V2" }
        ]
      }
    ]
  },
  {
    value: "3371",
    label: "工作",
    children: [
      {
        value: "3430",
        label: "智能笔",
        children: [
          { value: "3431", label: "智能笔V1" },
          { value: "3432", label: "智能笔V2" }
        ]
      }
    ]
  }
];
const displayRender = labels => {
  return labels.reduce((ret, label, index) => {
    ret.push(
      <span className="label-ellipsis" title={label}>
        {label}
      </span>
    );
    if (index < labels.length - 1) {
      ret.push(<span className="label-separator"> / </span>);
    }
    return ret;
  }, []);
};

export default class SimpleFluencyForm extends Component {
  static displayName = 'SimpleFluencyForm';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      step: 0,
      footerAlign: "left",
      visible: false
    };
  }
  map = ["left", "right", "center"];
  onClose = () => {
    this.setState({
      visible: false
    });
  };

  onOpen = () => {
    this.setState({
      visible: true
    });
  };

  // ICE: React Component 的生命周期

  componentWillMount() { }

  componentDidMount() { }

  componentWillReceiveProps() { }

  componentWillUnmount() { }

  formChange = (newValue) => {
    this.setState({
    });
  };

  nextStep = () => {
    this.setState({ step: this.state.step + 1 });
  };

  renderStep = (step) => {
    if (step === 0) {

      return (

        <IceContainer style={styles.form}>
          <FormBinderWrapper
            ref={(form) => {
              this.form = form;
            }}
            onChange={this.formChange}
          >
            <div>
              <Row style={styles.formRow}>
                <Col offset={7}>
                  <CascaderSelect
                    style={{ width: "240px" }}
                    displayRender={displayRender}
                    showItemCount="5"
                    labelWidth="80"
                    defaultValue="2975"
                    dataSource={dataSource}
                  />
                </Col>
              </Row>

              <Row>
                <Col offset={7}>
                  <Button onClick={this.nextStep} type="primary">
                    下一步
                  </Button>
                </Col>
              </Row>
            </div>
          </FormBinderWrapper>
        </IceContainer>
      );
    } else if (step === 1) {
      return (
        <IceContainer style={styles.form}>
          <FormBinderWrapper
            ref={(form) => {
              this.form = form;
            }}
          >
            <div>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  规格分类：生活/雾霾盒子
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  规格名称：
                </Col>
                <Col s="14" l="12">
                  <FormBinder type="text">
                    <Input
                      name="name"
                      size="large"
                      style={{ width: '100%' }}
                    />
                  </FormBinder>
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  规格icon：
                </Col>
                <Col s="14" l="12">
                  <DragUpload
                    action="//next-upload.shuttle.alibaba.net/upload" // 该接口仅作测试使用，业务请勿使用
                    accept="image/png, image/jpg, image/jpeg, image/gif, image/bmp"
                  />
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  规格描述：
                </Col>
                <Col s="14" l="12">
                  <Input multiple placeholder="multiple" />
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  规格备注：
                </Col>
                <Col s="14" l="12">
                  <Input multiple placeholder="multiple" />
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  更多图片：
                </Col>
                <Col s="14" l="12">
                  <ImageUpload
                    listType="picture-card"
                    action="//next-upload.shuttle.alibaba.net/upload" // 该接口仅作测试使用，业务请勿使用
                    accept="image/png, image/jpg, image/jpeg, image/gif, image/bmp"
                    locale={{
                      image: {
                        cancel: "取消上传",
                        addPhoto: "上传图片"
                      }
                    }}
                    defaultFileList={[
                      {
                        name: "IMG.png",
                        status: "done",
                        downloadURL:
                          "https://img.alicdn.com/tps/TB19O79MVXXXXcZXVXXXXXXXXXX-1024-1024.jpg",
                        fileURL:
                          "https://img.alicdn.com/tps/TB19O79MVXXXXcZXVXXXXXXXXXX-1024-1024.jpg",
                        imgURL:
                          "https://img.alicdn.com/tps/TB19O79MVXXXXcZXVXXXXXXXXXX-1024-1024.jpg"
                      }
                    ]}
                  />
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  标签（选填）
                </Col>
                <Col s="14" l="12">
                  <div>
                    <Tag shape="deletable">Tag1</Tag>
                    <Tag shape="deletable" animation={false}>
                      Tag2
                    </Tag>
                  </div>
                </Col>
              </Row>
              <Row>

                <Col offset={7}>
                  <Button onClick={this.nextStep} type="primary">
                    下一步
                  </Button>
                </Col>
              </Row>
            </div>

          </FormBinderWrapper>

        </IceContainer>
      );
    } else if (step === 2) {
      return (
        <IceContainer style={styles.form}>
          <FormBinderWrapper
            ref={(form) => {
              this.form = form;
            }}
          >
            <div>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  规格分类：生活/雾霾盒子/盒子一
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  标识前缀：
                </Col>
                <Col s="14" l="12">
                  <FormBinder type="text">
                    <Input
                      name="name"
                      size="large"
                      style={{ width: '100%' }}
                    />
                  </FormBinder>
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  scada模型：
                </Col>
                <Col s="14" l="12">
                  <DragUpload
                    action="//next-upload.shuttle.alibaba.net/upload" // 该接口仅作测试使用，业务请勿使用
                    accept="image/png, image/jpg, image/jpeg, image/gif, image/bmp"
                  />
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  状态：
                </Col>
                <Col s="14" l="12">
                  <Switch checkedChildren="开" unCheckedChildren="关" />
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  属性：
                </Col>
                <Col s="14" l="12">
                  <FormBinder type="text">
                    <Input
                      name="prop1"
                      size="large"
                      style={{ width: '30%' }}
                    />
                  </FormBinder>
                  --
                  <FormBinder type="text">
                    <Input
                      name="prop2"
                      size="large"
                      style={{ width: '65%' }}
                    />
                  </FormBinder>
                </Col>
              </Row>
              <Row>

                <Col offset={7}>
                  <Button onClick={this.nextStep} type="primary">
                    下一步
                  </Button>
                </Col>
              </Row>
            </div>
          </FormBinderWrapper>
        </IceContainer>
      );
    }
    else if (step === 3) {
      const footer = (
        <a onClick={this.onClose} href="javascript:;">
        </a>
      )
      return (
        <IceContainer style={styles.form}>
          <FormBinderWrapper
            ref={(form) => {
              this.form = form;
            }}
          >
            <div>
              <Row style={styles.formRow}>
                <Col xxs="5" s="5" l="7" style={styles.formLabel}>
                  规格分类：生活/雾霾盒子
                </Col>
              </Row>
              <Row style={styles.formRow}>
                <Col span={24} style={styles.centerText}>
                  <Button onClick={this.onOpen} style={{ width: '80%' }} type='normal'><Icon type="add" />添加</Button>
                </Col>
              </Row>
              <Row>
                <Col>
                  <SelectableTable />
                </Col>
              </Row>
              <Col offset={7}>
                <Button type="primary">
                  完成
                  </Button>
              </Col>
              <Dialog
                visible={this.state.visible}
                onOk={this.onClose}
                onCancel={this.onClose}
                onClose={this.onClose}
                footer={footer}
                title="字段/字段属性"
                footerAlign={this.state.footerAlign}
                style={{width:'400px'}}
              >
                <SettingsForm/>
              </Dialog>
            </div>
          </FormBinderWrapper>
        </IceContainer>
      );
    }
  };

  render() {
    return (
      <div className="simple-fluency-form">
        <IceContainer>
          <Step current={this.state.step} type="dot">
            <Step.Item key={0} title="选择分类" />
            <Step.Item key={1} title="基本信息" />
            <Step.Item key={2} title="高级信息" />
            <Step.Item key={3} title="动态字段" />
          </Step>
        </IceContainer>
        {this.renderStep(this.state.step)}
      </div>
    );
  }
}

const styles = {
  form: {
    padding: '40px 20px',
  },
  formLabel: {
    textAlign: 'right',
    lineHeight: '1.7rem',
    paddingRight: '10px',
  },
  centerText: {
    textAlign: 'center',
    lineHeight: '1.7rem',
    paddingRight: '10px',
  },
  formRow: {
    marginBottom: '20px',
  },
  formErrorWrapper: {
    marginTop: '5px',
  },
  simpleFluencyForm: {},
};

import React, { Component } from 'react';
import CustomBreadcrumb from '../../components/CustomBreadcrumb';
import { Card, Grid } from '@icedesign/base';
import SimpleFluencyForm from './components/SimpleFluencyForm';
const { Row, Col } = Grid;
export default class AddSpecification extends Component {
  static displayName = 'AddSpecification';

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const breadcrumb = [
      { text: '规格管理', link: '' },
      { text: '添加规格', link: '#/spec/add' },
    ];
    return (
      <div>
        <CustomBreadcrumb dataSource={breadcrumb} />
        <Row>
          <Col span={24}>
            <SimpleFluencyForm />
          </Col>
        </Row>
      </div>
    );
  }
}

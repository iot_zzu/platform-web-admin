import React, { Component } from 'react';
import CustomBreadcrumb from '../../components/CustomBreadcrumb';
import ContentEditor from './components/ContentEditor'
import { Card, Grid } from '@icedesign/base';
const { Row, Col } = Grid;
export default class AddSpecification extends Component {
  static displayName = 'AddSpecification';

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const breadcrumb = [
      { text: '字段管理', link: '' },
      { text: '编辑字段', link: '#/spec/addfield' },
    ];
    return (
      <div>
        <CustomBreadcrumb dataSource={breadcrumb} />
        <Row>
          <Col span={24}>
            <ContentEditor />
          </Col>
        </Row>
      </div>
    );
  }
}

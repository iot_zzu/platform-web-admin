import React, { Component } from 'react';
import IceContainer from '@icedesign/container';
import { Input, Grid, Form, Button, Select } from '@icedesign/base';
import Img from '@icedesign/img';
import {
  FormBinderWrapper as IceFormBinderWrapper,
  FormBinder as IceFormBinder,
  FormError as IceFormError,
} from '@icedesign/form-binder';

// import RichEditor from './RichEditor';

const { Row, Col } = Grid;
const FormItem = Form.Item;
const dataSource = {};
export default class ContentEditor extends Component {
  static displayName = 'ContentEditor';

  static propTypes = {};

  static defaultProps = {};

  
  constructor(props) {
    super(props);
    this.state = {
      value: {
        title: '',
        desc: '',
        // author: '',
        mockData: '',
        deal: '',
        cats: [],
        icon:'',
      },
      // dataSource: {src: 'https://img.alicdn.com/tfs/TB1zHh_dQyWBuNjy0FpXXassXXa-1350-900.jpg'},
    };
  }

  formChange = (value) => {
    console.log('value', value);
    this.setState({
      value,
    });
  };

  handleSubmit = () => {
    this.postForm.validateAll((errors, values) => {
      console.log('errors', errors, 'values', values);
      if (errors) {
        return false;
      }

      // ajax values
    });
  };

  // renderCover = (value, record) => {
  //   let type = 'cover';
  //   return (
  //     <div>
  //       <Img
  //         enableAliCDNSuffix={true}
  //         width={200}
  //         height={100}
  //         src={'record.src'}
  //         type={type}
  //         border={'1px'}
  //       />
  //     </div>
  //   );
  // };

  render() {
    return (
      <div className="content-editor">
        <IceFormBinderWrapper
          ref={(refInstance) => {
            this.postForm = refInstance;
          }}
          value={this.state.value}
          onChange={this.formChange}
        >
          <IceContainer title="设备规格基本信息">
            <Form labelAlign="top" style={styles.form}>
              <Row>
                <Col span="11">
                  <FormItem label="属性名称" required>
                    <IceFormBinder name="title" required message="名称必填">
                      <Input placeholder="这里填写属性名称" />
                    </IceFormBinder>
                    <IceFormError name="title" />
                  </FormItem>
                </Col>
              </Row>
              <Row>                  
                <Col span="11" >
                  <FormItem label="分类" required>
                    <IceFormBinder
                      name="cats"
                    >
                      <Select
                        style={styles.cats}
                        placeholder="请选择分类"
                        dataSource={[
                          { label: '分类1', value: 'cat1' },
                          { label: '分类2', value: 'cat2' },
                          { label: '分类3', value: 'cat3' },
                        ]}
                      />
                    </IceFormBinder>
                    <IceFormError
                      name="cats"
                      render={(errors) => {
                        console.log('errors', errors);
                        return (
                          <div>
                            <span style={{ color: 'red' }}>
                              {errors.map((item) => item.message).join(',')}
                            </span>
                          </div>
                        );
                      }}
                    />
                  </FormItem>
                </Col>
                <Col span="11">
                  <FormItem label="实时预览" style={styles.pre}>
                    <IceFormBinder
                      name="preview"
                    >
                      <div  style={styles.preview}></div>
                    </IceFormBinder>
                    <IceFormError name="preview" />
                  </FormItem>
                </Col>
              </Row>
              <FormItem label="规格描述：">
                <IceFormBinder name="desc">
                  <Input multiple placeholder="这里填写规格描述" />
                </IceFormBinder>
              </FormItem>
              <FormItem label="模拟数据：">
                <IceFormBinder name="mockData">
                  <Input multiple placeholder="这里填写模拟数据" />
                </IceFormBinder>
              </FormItem>
              <FormItem label="数据处理：">
                <IceFormBinder name="deal">
                  <Input multiple placeholder="这里填写数据处理" />
                </IceFormBinder>
              </FormItem>
              <FormItem label="图标代码：">
                <IceFormBinder name="icon">
                  <Input multiple placeholder="这里填写图标代码" />
                </IceFormBinder>
              </FormItem>
              {/* <FormItem label="正文" required>
                <IceFormBinder name="body">
                  <RichEditor />
                </IceFormBinder>
              </FormItem> */}
              <FormItem label=" ">
                <Button type="primary" onClick={this.handleSubmit}>
                  保存
                </Button>
              </FormItem>
            </Form>
          </IceContainer>
        </IceFormBinderWrapper>
      </div>
    );
  }
}

const styles = {
  form: {
    margin: '20px 150px',
  },
  cats: {
    width: '100%',
  },
  preview: {
    width: '200px',
    height: '40px',
    border: '1px solid',
  },
  pre: {
    marginLeft: '20px',
  }
};



import React, { Component } from 'react';
import TimeFilterTable from '../UserList/components/TimeFilterTable';
import StatisticalCard from '../UserList/components/StatisticalCard';
import TabTable from '../UserList/components/TabTable';
import SimpleSlide from '../UserList/components/SimpleSlider';
import CustomBreadcrumb from '../../components/CustomBreadcrumb';
import MainData from '../UserList/components/MainData';
import SelectableTable from '../UserList/components/SelectableTable'
import { Tab,Grid } from "@icedesign/base";
import '@icedesign/data-binder';
const {Row,Col} = Grid;
//tab选项卡
const TabPane = Tab.TabPane;

const tabs = [
  { tab: "智能灯(3)", key: "home", content: <SelectableTable /> },
  { tab: "雾霾盒子(17)", key: "doc", content: <SelectableTable /> },
  { tab: "智能花盆(64)", key: "api", content: <SelectableTable /> }
];

function handleChange(key) {
  console.log("change", key);
}

function handleClick(key) {
  console.log("click", key);
}


//
export default class UserList extends Component {
  static displayName = 'UserDetail';

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const breadcrumb = [
      { text: '用户管理', link: '' },
      { text: '用户详情', link: '#/user/detial' },
    ];
    return (
      <div className="create-user-page">
      <CustomBreadcrumb dataSource={breadcrumb}/>  
        <StatisticalCard />
        <MainData />
          <Row style={{margin:'30px 0',background:'#fff',borderRadius:'8px'}}>
            <img
              style={{ width: '100%' }}
              src="https://gw.alipayobjects.com/zos/rmsportal/HBWnDEUXCnGnGrRfrpKa.png" alt="" />
          </Row>
        <Tab onChange={handleChange} style={{background:'#fff'}}>
          {tabs.map(item => (
            <TabPane key={item.key} tab={item.tab} onClick={handleClick}>
              {item.content}
            </TabPane>
            ))}
        </Tab>
        <TimeFilterTable />
      </div>
    );
  }
}

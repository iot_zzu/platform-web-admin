import React, { Component } from 'react';
import { Link } from 'react-router';
import { Grid, Button, Tab, Tag, Table } from '@icedesign/base';
import MainData from './components/MainData';
import UserStatChart from './components/UserStatChart';
import TimeFilterTable from './components/TimeFilterTable';
import ChartPie from './components/ChartPie';
import SpecDetailInfo from './../SpecDetailInfo/index';
const TabPane = Tab.TabPane;
const { Row, Col } = Grid;


function handleChange(key) {
  console.log("change", key);
}

function handleClick(key) {
  console.log("click", key);
}

const onRowClick = function (record, index, e) {
  console.log(record, index, e);
},
  getData = () => {
    let result = [];
    for (let i = 0; i < 5; i++) {
      result.push({
        title: {
          name: `河南省郑州市金水区`
        },
        id: 'boxa-boxa-01-00000001',
        time: '无法检测温度'
      });
    }
    return result;
  },
  render = (value, index, record) => {
    return <a>Remove({record.id})</a>;
  };



export default class SpecDetailTotal extends Component {
  static displayName = 'SpecDetailTotal';

  constructor(props) {
    super(props);
    this.state = {};
  }
 
  render() {
    const tabs = [
      {
        
        tab: "总览", key: "home", content: <div>
          <MainData />
          <Row style={{height: 350,marginTop:30}}>
            <img
              style={{ width: '100%' }}
              src="https://gw.alipayobjects.com/zos/rmsportal/HBWnDEUXCnGnGrRfrpKa.png" alt="" />
          </Row>
          <Row guntter={20} style={{ height: 450 ,marginTop:30}}>
            <Col span={8} style={{ marginRight: 15, backgroundColor: '#fff' }}>
              <ChartPie />
            </Col>
            <Col span={16} style={{ backgroundColor: '#fff' }}>
              <UserStatChart />
            </Col>
          </Row>

          <h2 style={{ background: '#fff', borderBottom: '1px solid #E9E9E9', fontWeight: 600,marginTop:'30px' }}>异常设备</h2>
          <Row style={{marginTop:30}}>
            <Table dataSource={getData()} onRowClick={onRowClick}>
              <Table.Column title="硬件标识" dataIndex="id" />
              <Table.Column title="位置" dataIndex="title.name" />
              <Table.Column title="异常信息" dataIndex="time" />
              <Table.Column cell={render} width="40%" />
            </Table>
          </Row>

          <TimeFilterTable />
        </div>
      },
      {
        tab: "详情", key: "doc", content: 
          <div>
      
      <SpecDetailInfo/>      
    </div>
      },
    ];
    return <div className="spec-detail-total-page">
      <h2 style={{ color: '#000', backgroundColor: '#fff', marginBottom: -8, paddingLeft: 15, paddingTop: 10, fontWeight: 700 }}>规格名称：雾霾盒子</h2>
      <Row style={{ backgroundColor: '#fff', height: 180, padding: '5px 50px ' }}>
        <Col span={5} style={{ paddingTop: 30, textAlign: 'left' }}>
          <p style={{ fontSize: '16px', color: '#000' }}>创建人:<span>曲丽丽</span></p>
          <p style={{ fontSize: '16px', color: '#000' }}>创建时间:<span>2017-2-22</span></p>
          <p style={{ fontSize: '16px', color: '#000' }}>更新时间:<span>2017-2-22</span></p>
        </Col>
        <Col span={5} style={{ paddingTop: 30, textAlign: 'left' }}>
          <p style={{ fontSize: '14px', color: '#000' }}>规格编号：<span>spec-spec-spec-spec-spec</span></p>
          <p style={{ fontSize: '14px', color: '#000' }}>规格分类：<span>生活/居家</span></p>
          <p style={{ fontSize: '14px', color: '#000' }}>规格备注：雾霾盒子是真的好<span>2017-2-22</span></p>
        </Col>
        <Col span={10} style={{ paddingTop: 40, textAlign: 'left', paddingLeft: 15 }}>
          <p style={{ fontSize: '14px', color: '#000' }}>标识前缀：<span>bo01xa-boxa-</span></p>
          <p style={{ fontSize: '14px', color: '#000', verticalAlign: 'center' }}>规格标签：<Tag>tag1</Tag></p>
        </Col>
        <Col span={4} style={{ paddingTop: 30, textAlign: 'right' }}>
         <Link to="spec/edit"> <Button type="primary">编辑</Button></Link>
          </Col>
      </Row>
      <Tab onChange={handleChange} style={{ background: '#fff', marginBottom: 30 }}>
        {tabs.map(item => (
          <TabPane key={item.key} tab={item.tab} onClick={handleClick}>
            {item.content}
          </TabPane>
        ))}
      </Tab>
    </div>;
  }
}

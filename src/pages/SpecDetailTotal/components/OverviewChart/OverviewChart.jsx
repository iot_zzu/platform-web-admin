import React, { Component } from 'react';
import IceContainer from '@icedesign/container';
import { Grid } from '@icedesign/base';
import PieDonutChart from './PieDonutChart';
import UserStartChart from '../UserStatChart'
import BarChart from './BarChart';
import LineChart from './LineChart';

const { Row, Col } = Grid;

export default class OverviewChart extends Component {
  static displayName = 'OverviewChart';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <IceContainer>
        <Row wrap gutter="20" style={styles.overviewChart}>
          <Col xxs="24" s="8" l="8" span={8}>
            <IceContainer title="">
              <PieDonutChart />
            </IceContainer>
          </Col>
          <Col xxs="24" s="8" l="8" span={48}>
            <UserStartChart />
          </Col>

          {/* <Col xxs="24" s="8" l="8" span={16}>
            <IceContainer title="">
              <BarChart />
            </IceContainer>
          </Col> */}
          {/* <Col xxs="24" s="8" l="8">
            <IceContainer title="">
              <LineChart />
            </IceContainer>
          </Col> */}
        </Row>
      </IceContainer>
    );
  }
}

const styles = {};

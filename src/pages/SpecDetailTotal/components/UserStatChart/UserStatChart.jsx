import React, { Component } from 'react';
import { Chart, Geom, Axis, Tooltip, Legend, Coord, Label } from 'bizcharts';
import { Grid } from '@icedesign/base';
import IceContainer from '@icedesign/container';

const { Row, Col } = Grid;

export default class UserStatChart extends Component {
  static displayName = 'UserStatChart';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="user-stat-chart">
        <Row wrap gutter="20">
          <Col style={{width:'100%'}}>
            <IceContainer title="用户增长数量">
              <Chart
                height={350}
                data={userData}
                forceFit
                padding={[40, 40, 40, 40]}
              >
                <Axis name="month" />
                <Axis name="count" />
                <Tooltip crosshairs={{ type: 'y' }} />
                <Geom type="interval" position="month*count" />
              </Chart>
            </IceContainer>
            </Col>
        </Row>
      </div>
    );
  }
}

const sexRatio = [
  {
    item: '男',
    percent: 57,
  },
  {
    item: '女',
    percent: 30,
  },
  {
    item: '其他',
    percent: 13,
  },
];

const userData = [
  {
    month: '1 月',
    count: 50,
  },
  {
    month: '2 月',
    count: 60,
  },
  {
    month: '3 月',
    count: 120,
  },
  {
    month: '4 月',
    count: 90,
  },
  {
    month: '5 月',
    count: 100,
  },
  {
    month: '6 月',
    count: 300,
  },
  {
    month: '7 月',
    count: 110,
  },
  {
    month: '8 月',
    count: 320,
  },
  {
    month: '9 月',
    count: 260,
  },
  {
    month: '10 月',
    count: 220,
  },
  {
    month: '11 月',
    count: 420,
  },
  {
    month: '12 月',
    count: 320,
  },
];
const ageData = [
  {
    age: '15 岁',
    count: 10,
  },
  {
    age: '16 岁',
    count: 220,
  },
  {
    age: '17 岁',
    count: 200,
  },
  {
    age: '18 岁',
    count: 530,
  },
  {
    age: '19 岁',
    count: 140,
  },
  {
    age: '20 岁',
    count: 1030,
  },
  {
    age: '21 岁',
    count: 130,
  },
];

import React, { Component } from 'react';
import ContentEditor from './components/ContentEditor';
import CustomBreadcrumb from '../../components/CustomBreadcrumb';
import SelectableTable from './components/SelectableTable'
import ColumnForm from './components/ColumnForm'
import { Card, Grid, Button, DatePicker, Upload, Field, Input, Select } from "@icedesign/base";

const { Row, Col } = Grid;

export default class SpecificationField extends Component {
  static displayName = 'SpecificationField';
  field = new Field(this);

  constructor(props) {
    super(props);
    this.state = {};
  }

  normDate(date, strdate) {
    // console.log("normDate:", date, strdate);
    return strdate;
  }

  render() {
    const { init, setValue, reset } = this.field;
    const breadcrumb = [
      { text: '规格管理', link: '' },
      { text: '字段管理', link: '#/spec/field' },
    ];
    return (
      <div>
        <CustomBreadcrumb dataSource={breadcrumb} />
        <Row>
          <Col span={24}>
            <ColumnForm />
            <SelectableTable />
          </Col>    
        </Row>
      </div>
    );
  }
}

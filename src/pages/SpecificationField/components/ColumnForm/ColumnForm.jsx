import React, { Component } from 'react';
import IceContainer from '@icedesign/container';
import {
  FormBinderWrapper as IceFormBinderWrapper,
  FormBinder as IceFormBinder,
  FormError as IceFormError,
} from '@icedesign/form-binder';
import { Input, Button, Select, Grid, DatePicker } from '@icedesign/base';

const { Row, Col } = Grid;

export default class ColumnForm extends Component {
  static displayName = 'ColumnForm';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      value: {
        attributeName: '',
        attributeDes: '',
        settleAccount: '',
        attributeStatus: '',
        date: '',
      },
    };
  }

  onFormChange = (value) => {
    this.setState({
      value,
    });
  };

  reset = () => {
    this.setState({
      value: {
        attributeName: '',
        attributeDes: '',
        settleAccount: '',
        attributeStatus: '',
        date: 'usd',
      },
    });
  };

  submit = () => {
    this.formRef.validateAll((error, value) => {
      console.log('error', error, 'value', value);
      if (error) {
        // 处理表单报错
      }
      // 提交当前填写的数据
    });
  };

  render() {
    return (
      <div className="column-form">
        <IceContainer title="属性信息" style={styles.container}>
          <IceFormBinderWrapper
            ref={(formRef) => {
              this.formRef = formRef;
            }}
            value={this.state.value}
            onChange={this.onFormChange}
          >
            <div>
              <Row wrap>
                <Col xxs="24" s="12" l="12">
                  <Row style={styles.formItem}>
                    <Col xxs="8" s="6" l="4" style={styles.formLabel}>
                      属性名称：
                    </Col>

                    <Col s="12" l="12">
                      <IceFormBinder
                        name="attributeName"
                        required
                        message="属性名称必须填写"
                      >
                        <Input style={styles.inputcss} />
                      </IceFormBinder>
                      <IceFormError name="attributeName" />
                    </Col>
                  </Row>

                  <Row style={styles.formItem}>
                    <Col xxs="8" s="6" l="4" style={styles.formLabel}>
                      属性描述：
                    </Col>
                    <Col s="12" l="12">
                      <IceFormBinder
                        name="attributeDes"
                        required
                        message="属性描述必须填写"
                      >
                        <Input style={styles.inputcss} />
                      </IceFormBinder>
                      <IceFormError name="attributeDes" />
                    </Col>
                  </Row>

                  
                </Col>

                <Col xxs="24" s="12" l="12">
                <Row style={styles.formItem}>
                    <Col xxs="8" s="6" l="4" style={styles.formLabel}>
                      属性状态：
                    </Col>
                    <Col s="12" l="12">
                      <IceFormBinder name="attributeStatus">
                        <Select
                          className="next-form-text-align"
                          style={styles.inputcss}
                          required
                          message="请选择状态"
                          dataSource={[
                            { label: '状态1', value: 'status1' },
                            { label: '状态2', value: 'status2' },
                            { label: '状态3', value: 'status3' },
                          ]}
                        />
                      </IceFormBinder>
                      <IceFormError name="attributeStatus" />
                    </Col>
                  </Row>

                  <Row style={styles.formItem}>
                    <Col xxs="8" s="6" l="4" style={styles.formLabel}>
                      创建日期：
                    </Col>
                    <Col s="12" l="12">
                      <IceFormBinder name="data">
                        <DatePicker 
                          style={styles.inputcss}
                          className="next-form-text-align"
                        />
                      </IceFormBinder>
                    </Col>
                  </Row>
                </Col>
              </Row>

              <Row style={styles.btns}>
                <Col xxs="8" s="2" l="2" style={styles.formLabel}>
                  {' '}
                </Col>
                <Col s="12" l="10">
                  <Button type="primary" onClick={this.submit}>
                    搜索
                  </Button>
                  <Button style={styles.resetBtn} onClick={this.reset}>
                    重置
                  </Button>
                </Col>
              </Row>
            </div>
          </IceFormBinderWrapper>
        </IceContainer>
      </div>
    );
  }
}

const styles = {
  container: {
    paddingBottom: 0,
  },
  formItem: {
    height: '28px',
    lineHeight: '28px',
    marginLeft: '50px',
    marginBottom: '30px',
  },
  formLabel: {
    textAlign: 'right',
  },
  btns: {
    margin: '25px 0',
  },
  resetBtn: {
    marginLeft: '20px',
  },
  inputcss: {
    width: '100%',
  }
};

import React, { Component } from 'react';
import { Dialog, Button, Form, Input, Field, Select } from '@icedesign/base';

const FormItem = Form.Item;

export default class EditDialog extends Component {
  static displayName = 'EditDialog';

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      dataIndex: null,
    };
    this.field = new Field(this);
  }

  handleSubmit = () => {
    this.field.validate((errors, values) => {
      if (errors) {
        console.log('Errors in form!!!');
        return;
      }

      const { dataIndex } = this.state;
      console.log(dataIndex, values);
      this.props.getFormValues(dataIndex, values);
      this.setState({
        visible: false,
      });
    });
  };

  onOpen = (index, record) => {
    this.field.setValues({ ...record });
    this.setState({
      visible: true,
      dataIndex: index,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  render() {
    const init = this.field.init;
    const { index, record } = this.props;
    const formItemLayout = {
      labelCol: {
        fixedSpan: 6,
      },
      wrapperCol: {
        span: 14,
      },
    };

    return (
      <div style={styles.editDialog}>
        <Button
          // size="small"
          type="primary"
          onClick={() => this.onOpen(index, record)}
        >
          编辑
        </Button>
        <Dialog
          style={{ width: 640 }}
          visible={this.state.visible}
          onOk={this.handleSubmit}
          closable="esc,mask,close"
          onCancel={this.onClose}
          onClose={this.onClose}
          title="编辑"
        >
          <Form direction="ver" field={this.field}>
            <FormItem label="id编号：" {...formItemLayout}>
              <Input
                disabled
                {...init('id', {
                  rules: [{ required: true, message: '必填选项' }],
                })}
              />
            </FormItem>

            <FormItem label="名称：" {...formItemLayout}>
              <Input
                {...init('title', {
                  rules: [{ required: true, message: '必填选项' }],
                })}
              />
            </FormItem>

            <FormItem label="模板：" {...formItemLayout}>
              <Select
                {...init('template', {
                  rules: [{ required: true, message: '必填选项' }],
                })}
                >
                <Option value="常用">常用</Option>
                <Option value="不常用">不常用</Option>
              </Select>
            </FormItem>

            <FormItem label="状态：" {...formItemLayout}>
              {/* <Input
                
                {...init('status', {
                  rules: [{ required: true, message: '必填选项' }],
                })}
              /> */}
              <Select
                {...init('status', {
                  rules: [{ required: true, message: '必填选项' }],
                })}
                >
                <Option value="启用">启用</Option>
                <Option value="禁止">禁止</Option>
              </Select>
            </FormItem>

            <FormItem label="操作者：" {...formItemLayout}>
              <Input
                
                {...init('publisher', {
                  rules: [{ required: true, message: '必填选项' }],
                })}
              />
            </FormItem>

            <FormItem label="注册时间：" {...formItemLayout}>
              <Input
               
                {...init('time', {
                  rules: [{ required: true, message: '必填选项' }],
                })}
              />
            </FormItem>

            {/* <FormItem label="最后登录时间：" {...formItemLayout}>
              <Input
                disabled
                {...init('LastLoginTime', {
                  rules: [{ required: true, message: '必填选项' }],
                })}
              />
            </FormItem> */}
          </Form>
        </Dialog>
      </div>
    );
  }
}

const styles = {
  editDialog: {
    display: 'inline-block',
    marginRight: '5px',
  },
};

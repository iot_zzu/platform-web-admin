import React, { Component } from 'react';
import { Link } from 'react-router';
import { Table, Button, Icon, Pagination } from '@icedesign/base';
import IceContainer from '@icedesign/container';
import EditDialog from './components/EditDialog'
import Img from '@icedesign/img';

const getMockData = () => {
  const result = [];
  for (let i = 0; i < 10; i++) {
    result.push({
      id: 1000 + i,
      title: 
        // name: '折线图',
        <div>      
          <ul>
            <li><b>折线图</b></li>
            <li>适合用来展示大量数据的折线图</li>
          </ul> 
        </div>,
      // src: 'http://112.74.168.74:8889/images/map/u108.png',
      template: '常用',
      status: '启用',
      publisher: 'User',
      time: '2000-10-1' + i,
    });
  }
  return result;
};

// 注意：下载数据的功能，强烈推荐通过接口实现数据输出，并下载
// 因为这样可以有下载鉴权和日志记录，包括当前能不能下载，以及谁下载了什么

export default class SelectableTable extends Component {
  static displayName = 'SelectableTable';

  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    dataSource: getMockData();
    // 表格可以勾选配置项
    this.rowSelection = { 
      // 表格发生勾选状态变化时触发
      onChange: (ids) => {
        console.log('ids', ids);
        this.setState({
          selectedRowKeys: ids,
        });
      },
      // 全选表格时触发的回调
      onSelectAll: (selected, records) => {
        console.log('onSelectAll', selected, records);
      },
    };

    this.state = {
      selectedRowKeys: [],
      dataSource: getMockData(),
    };
  }

  clearSelectedKeys = () => {
    this.setState({
      selectedRowKeys: [],
    });
  };

  deleteSelectedKeys = () => {
    const { selectedRowKeys, dataSource } = this.state;
    console.log('delete keys', selectedRowKeys);
    dataSource.splice(selectedRowKeys, 1);
    this.setState({
      dataSource,
    });
  };

  deleteItem = (record) => {
    const { id } = record;
    console.log('delete item', id);
  };
  add = () => {
    window.location.href="#/spec/addfield";
  }
  renderOperator = (value, index, record) => {
    return (
      <span>
        <EditDialog
          index={index}
          record={record}
          getFormValues={this.getFormValues}
        />
        {/* <DeleteBalloon
          handleRemove={() => this.handleRemove(value, index, record)}
        />*/}
      </span> 
    );
  };

  getFormValues = (dataIndex, values) => {
    const { dataSource } = this.state;
    dataSource[dataIndex] = values;
    this.setState({
      dataSource,
    });
  };
  
  renderCover = () => {
    // let type = cover;
    // if (index % 3 === 0) {
    //   type = "contain";
    // }
    return (
      <div style={styles.divImg}>
        <Img
          enableAliCDNSuffix={true}
          style={styles.img}
          src={'http://112.74.168.74:8889/images/map/u108.png'}
          // type={type}
        />
      </div>
    );
  };

  onSort(dataIndex, order) {
    let dataSource = this.state.dataSource.sort(function(a, b) {
      let result = a[dataIndex] - b[dataIndex];
      return order === "asc" ? (result > 0 ? 1 : -1) : result > 0 ? -1 : 1;
    });
    this.setState({
      dataSource
    });
  }

  render() {
    return (
      <div className="selectable-table" style={styles.selectableTable}>
        <IceContainer style={styles.IceContainer}>
          <div>
            <Button
              onClick={this.add}
              type="secondary"
              style={styles.batchBtn}             
            >
              <Icon type="add" />增加
            </Button>
            <Button
              onClick={this.deleteSelectedKeys}
              type="primary"
              style={styles.batchBtn}
              disabled={!this.state.selectedRowKeys.length}
            >
              <Icon type="ashbin" />删除
            </Button>
            <Button
              onClick={this.clearSelectedKeys}
              style={styles.batchBtn}
            >
              <Icon type="close" />清空选中
            </Button>
          </div>
        </IceContainer>
        <IceContainer>
          <Table
            dataSource={this.state.dataSource}
            isLoading={this.state.isLoading}
            onSort={this.onSort.bind(this)}
            rowSelection={{
              ...this.rowSelection,
              selectedRowKeys: this.state.selectedRowKeys,
            }}
          >
            <Table.Column title="id编号" dataIndex="id" sortable width={60} />
            <Table.Column title="名称" dataIndex="title" width={190} />
            <Table.Column title="类型" dataIndex="type" width={180} cell={this.renderCover}/>
            <Table.Column title="模板" dataIndex="template" width={40} />
            <Table.Column title="状态" dataIndex="status" width={40} />
            <Table.Column title="操作者" dataIndex="publisher" width={50} />
            <Table.Column title="修改时间" dataIndex="time" sortable width={80} />
            <Table.Column
              title="操作"
              cell={this.renderOperator}
              lock="right"
              width={60}
              
            />
          </Table>
          <div style={styles.pagination}>
            <Pagination onChange={this.change} />
          </div>
        </IceContainer>
      </div>
    );
  }
}

const styles = {
  batchBtn: {
    marginRight: '10px',
  },
  IceContainer: {
    marginBottom: '5px',
    minHeight: 'auto',
    display: 'flex',
    justifyContent: 'space-between',
  },
  removeBtn: {
    marginLeft: 10,
  },
  pagination: {
    textAlign: 'right',
    paddingTop: '26px',
  },
  divImg: {
    overflow:'hidden',
  },
};
